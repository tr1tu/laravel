<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Address;
use App\Group;
use App\Sizing;
use App\Size;
use App\Stock;
use App\Increase;
use App\Color;
use App\Role;
use App\Product;
use App\Category;
use App\Image;
use App\Discount;
use App\Tax;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('addresses')->delete();
        DB::table('users')->delete();
        DB::table('role_user')->delete();
        DB::table('roles')->delete();
        DB::table('stocks')->delete();
        DB::table('increases')->delete();
        DB::table('colors')->delete();
        DB::table('images')->delete();
        DB::table('discount_product')->delete();
        DB::table('product_tax')->delete();
        DB::table('discounts')->delete();
        DB::table('taxes')->delete();
        DB::table('products')->delete();
        DB::table('product_sizing')->delete();
        DB::table('sizes')->delete();
        DB::table('sizings')->delete();
        DB::table('categories')->delete();

      
        $roleUser = ['name' => 'User'];
        $roleAdmin = ['name' => 'Admin'];

        $roleUser = new Role($roleUser);
        $roleUser->save();
        $roleAdmin = new Role($roleAdmin);
        $roleAdmin->save();

        $users = array(
                ['username' => 'Ryan Chenkie', 'email' => 'ryanchenkie@gmail.com', 'password' => Hash::make('secret')],
                ['username' => 'Chris Sevilleja', 'email' => 'chris@scotch.io', 'password' => Hash::make('secret')],
                ['username' => 'Holly Lloyd', 'email' => 'holly@scotch.io', 'password' => Hash::make('secret')],
                ['username' => 'Adnan Kukic', 'email' => 'adnan@scotch.io', 'password' => Hash::make('secret')],
        );
            

        // Loop through each user above and create the record for them in the database
        foreach ($users as $user)
        {
            $user = User::create($user);


            $user->roles()->attach($roleAdmin->id);
            $user->save();

            $addresses = array(
                    ['description' => 'Descripcion de la direccion 1 de ' . $user->username],
                    ['description' => 'Descripcion de la direccion 2 de ' . $user->username]

            );

            foreach($addresses as $address)
            {
                $user->addresses()->save(new Address($address));
            }

        }

        for($i = 0; $i < 200; $i++)
        {

            echo 'Creating user ' . $i . "\n";

            $user = ['username' => 'TestUser' . $i, 'email' => 'TestEmail' . $i . '@gmail.com', 'password' => Hash::make('secret'),
                    'name' => 'Test Name ' . $i, 'lastname' => 'Test Last Name ' . $i, 'phone' => 1234567 + $i, 
                    'mobphone' => 1122334 + $i, 'fax' => 3211232 + $i, 'country' => 'TestCountry ' . $i];

            $user = User::create($user);

            $user->roles()->attach($roleUser->id);
            $user->save();

             $addresses = array(
                ['description' => 'Descripcion de la direccion 1 de ' . $user->username],
                ['description' => 'Descripcion de la direccion 2 de ' . $user->username]

            );

            foreach($addresses as $address)
            {
                $user->addresses()->save(new Address($address));
            }


        }

        /*$tags = [];

        for($i = 1; $i <= 100; $i++)
        {

            $tag = [ 'name' => 'Tag n ' . $i ];

            $tag = new Tag($tag);
            $tag->save();

            $tags[] = $tag;
        }*/

        $categories = [];

        for($i = 1; $i <= 15; $i++)
        {

            $category = ['name' => 'Categoria n ' . $i, 'description' => 'Descripcion de la categoria ' . $i, 'parent_id' => null,
                        'active' => rand(0,1)];

            $category = new Category($category);
            $category->save();

            /*for($x = 0; $x <= 99; $x = $x + rand(1, 13))
            {
                $category->addTag($tags[$x]->id);
            }*/

            $categories[] = $category;
        }

        $discounts = [];

        for($i = 1; $i <= 25; $i++)
        {
            $discount = [ 'name' => 'Discount n ' . $i, 'type' => rand(0,1), 'ammount' => rand(0,100), 'permanent' => rand(0,1),
            'start_date' => '2016-08-01', 'end_date' => '2017-08-01' ];
            $discount = new Discount($discount);
            $discount->save();

            $discounts[] = $discount;
           
        }

        $taxes = [];

        for($i = 1; $i <= 25; $i++)
        {
            $tax = [ 'name' => 'Tax n ' . $i, 'tax' => rand(1,100) ];
            $tax = new Tax($tax);
            $tax->save();

            $taxes[] = $tax;
           
        }


        for($i = 1; $i <= 10; $i++)
        {
            $sizing = ['sizing_number' => $i, 'name' => 'Tallaje n ' . $i];

            $sizing = Sizing::create($sizing);

            $sizes = [];

            for($j = 1; $j <= $i; $j++)
            {
                $size = ['size' => $j * 2];

                $size = new Size($size);
                $size->sizing()->associate($sizing);
                $size->save();

                array_push($sizes, $size);

            }

            for($k = 1; $k <= 50; $k++)
            {
                echo 'Creating product ' . $i . ' - ' . $k . "\n";

                $product = ['name' => 'Producto de ejemplo' . $i . $k, 'price' => rand(20,80),
                            'description' => 'Descripcion de ejemplo ' . $i . $k, 'reference' => 'Ref.' . $i . $k, 
                            'stock' => rand(0,100), 'category_id' => $categories[rand(0,14)]->id ];

                $product = new Product($product);
                $product->save();
                $product->sizings()->attach($sizing);
                

                foreach($sizes as $s)
                {
                    $stock = ['stock' => rand(0,50)];

                    $stock = new Stock($stock);
                    $stock->product()->associate($product);
                    $stock->size()->associate($s);
                    $stock->save();

                    $increase = ['increase' => rand(0,100), 'type' => rand(1,2)];

                    $increase = new Increase($increase);
                    $increase->product()->associate($product);
                    $increase->size()->associate($s);
                    $increase->save();
                    
                }

                for($x = 1; $x <= 5; $x++)
                {
                    $color = ['name' => 'Color ' . $i . $k . $x, 'image' => '', 'RGB' => '#2222FF'];

                    $color = new Color($color);
                    $product->colors()->save($color);
                }

                /*for($x = 0; $x <= 99; $x = $x + rand(1, 17))
                {
                    $product->addTag($tags[$x]->id);
                }*/

                $image = ['url' => 'test.jpg', 'description' => 'Imagen n ' . $i . "-" . $k];
                $image = new Image($image);
                $image->product()->associate($product);
                $image->save();

                for($x = 0; $x <= 24; $x = $x + rand(4,9))
                {
                    $product->discounts()->attach($discounts[$x]);
                }

                for($x = 0; $x <= 24; $x = $x + rand(4,9))
                {
                    $product->taxes()->attach($taxes[$x]);
                }

            }

        }




        Model::reguard();
    }
}

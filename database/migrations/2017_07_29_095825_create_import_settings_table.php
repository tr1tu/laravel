<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('import_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('import_profile_id')->unsigned();
            $table->string('key', 50);
            $table->string('value', 255);
            $table->timestamps();

            $table->unique(['import_profile_id', 'key']);

            $table->foreign('import_profile_id')->references('id')->on('import_profiles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('import_settings');
    }
}

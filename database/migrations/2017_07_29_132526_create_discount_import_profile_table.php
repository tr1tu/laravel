<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountImportProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discount_import_profile', function (Blueprint $table) {
            $table->integer('import_profile_id')->unsigned();
            $table->integer('discount_id')->unsigned();
            $table->timestamps();

            $table->primary(['import_profile_id', 'discount_id']);

            $table->foreign('import_profile_id')->references('id')->on('import_profiles')->onDelete('cascade');
            $table->foreign('discount_id')->references('id')->on('discounts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('discount_import_profile');
    }
}

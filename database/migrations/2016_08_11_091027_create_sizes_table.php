<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sizes', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('sizing_id')->unsigned();
            $table->string('size', 15);

            $table->timestamps();

            $table->foreign('sizing_id')->references('id')->on('sizings')->onUpdate('cascade')->onDelete('cascade');
            
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sizes');
    }
}

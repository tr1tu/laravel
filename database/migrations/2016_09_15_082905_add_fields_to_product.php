<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function(Blueprint $table) {
            $table->integer('code')->unsigned();
            $table->bigInteger('ean')->unsigned();
            $table->decimal('trading_price', 7, 2)->unsigned();
            $table->smallInteger('units_per_pack')->unsigned();
            $table->string('manufacturer', 100);
            $table->tinyInteger('discontinued')->unsigned();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function(Blueprint $table) {
            $table->dropColumn('code');
            $table->dropColumn('ean');
            $table->dropColumn('trading_price');
            $table->dropColumn('units_per_pack');
            $table->dropColumn('manufacturer');
            $table->dropColumn('discontinued');

        });
    }
}

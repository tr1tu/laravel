<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('import_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('supnodes', 200);
            $table->string('name_format', 200);
            $table->string('description_format', 200);
            $table->string('price_format', 200);
            $table->string('stock_format', 200);
            $table->string('reference_format', 200);
            $table->string('category_format', 200);
            $table->string('code_format', 200);
            $table->string('ean_format', 200);
            $table->string('trading_price_format', 200);
            $table->string('units_per_pack_format', 200);
            $table->string('manufacturer_format', 200);
            $table->string('discountinued_format', 200);
            $table->string('image_url_format', 200);
            $table->string('small_image_url_format', 200);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('import_profiles');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 20)->unique();
            $table->string('password', 64);
            $table->string('salt', 32);
            $table->string('email', 100)->unique();
            $table->string('name', 50);
            $table->string('lastname', 50);
            $table->string('dnicif', 10);
            $table->string('phone', 15);
            $table->string('mobphone', 15);
            $table->string('fax', 15);
            $table->integer('activated');
            $table->string('country', 50);       
            
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}

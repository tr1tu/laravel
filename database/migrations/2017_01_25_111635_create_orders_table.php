<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();

            //Billing address data
            $table->string('ba_name', 50);
            $table->string('ba_lastname', 50);
            $table->string('ba_dnicif', 10);
            $table->string('ba_country', 70);
            $table->string('ba_province', 50);
            $table->string('ba_city', 100);
            $table->string('ba_zip_code', 5);
            $table->string('ba_phone', 15);
            $table->string('ba_mob_phone', 15);
            $table->string('ba_address', 100);

            //Shipping address data
            $table->string('sa_name', 50);
            $table->string('sa_lastname', 50);
            $table->string('sa_dnicif', 10);
            $table->string('sa_country', 70);
            $table->string('sa_province', 50);
            $table->string('sa_city', 100);
            $table->string('sa_zip_code', 5);
            $table->string('sa_phone', 15);
            $table->string('sa_mob_phone', 15);
            $table->string('sa_address', 100);

            //Shipment data
            $table->string('sm_name', 100);
            $table->decimal('sm_fee', 7, 2)->unsigned();

            $table->decimal('price_subtotal', 15, 2);
            $table->decimal('price_total', 15, 2);

            $table->smallInteger('status')->unsigned();

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}

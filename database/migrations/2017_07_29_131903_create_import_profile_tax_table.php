<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportProfileTaxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('import_profile_tax', function (Blueprint $table) {
            $table->integer('import_profile_id')->unsigned();
            $table->integer('tax_id')->unsigned();
            $table->timestamps();

            $table->primary(['import_profile_id', 'tax_id']);

            $table->foreign('import_profile_id')->references('id')->on('import_profiles')->onDelete('cascade');
            $table->foreign('tax_id')->references('id')->on('taxes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('import_profile_tax');
    }
}

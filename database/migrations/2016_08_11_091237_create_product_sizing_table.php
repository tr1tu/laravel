<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductSizingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('product_sizing', function(Blueprint $table) {
            $table->integer('product_id')->unsigned();
            $table->integer('sizing_id')->unsigned();
            
            $table->primary(['product_id', 'sizing_id']);

            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('sizing_id')->references('id')->on('sizings')->onDelete('cascade');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_sizing');
    }
}

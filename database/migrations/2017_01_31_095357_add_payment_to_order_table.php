<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentToOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function(Blueprint $table) {
            $table->string('p_name', 100);
            $table->decimal('p_fee', 7, 2);
            $table->string('paypal_payment_id', 100)->default("");
            $table->string('paypal_token', 100)->default("");
            $table->string('paypal_payer_id', 50)->default("");
            $table->string('paypal_invoice_number', 100)->default("");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function(Blueprint $table) {
            $table->dropColumn('p_name');
            $table->dropColumn('p_fee');
            $table->dropColumn('paypal_payment_id');
            $table->dropColumn('paypal_token');
            $table->dropColumn('paypal_payer_id');
            $table->dropColumn('paypal_invoice_number');
        });
    }
}

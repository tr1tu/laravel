<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncreasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('increases', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('size_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->decimal('increase', 7, 2);
            $table->integer('type')->unsigned();
            $table->timestamps();

            $table->foreign('size_id')->references('id')->on('sizes')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('increases');
    }
}

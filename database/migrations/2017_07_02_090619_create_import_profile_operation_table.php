<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportProfileOperationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('import_profile_operation', function (Blueprint $table) {
            $table->integer('import_profile_id')->unsigned();
            $table->integer('operation_id')->unsigned();
            $table->integer('order')->unsigned();
            $table->timestamps();

            $table->primary(['import_profile_id', 'operation_id']);
            $table->unique(['import_profile_id', 'order']);

            $table->foreign('import_profile_id')->references('id')->on('import_profiles')->onDelete('cascade');
            $table->foreign('operation_id')->references('id')->on('operations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('import_profile_operation');
    }
}

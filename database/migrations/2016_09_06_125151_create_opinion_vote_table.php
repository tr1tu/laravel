<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpinionVoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opinion_votes', function(Blueprint $table) {

            $table->integer('opinion_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->tinyInteger('value')->unsigned();

            $table->primary(['opinion_id', 'user_id']);

            $table->foreign('opinion_id')->references('id')->on('opinions')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('opinion_votes');
    }
}

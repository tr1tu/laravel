<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemakeImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('images');
        Schema::create('images', function(Blueprint $table) {

            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->string('url', 512);
            $table->string('description', 256);
            $table->integer('order')->unsigned()->nullable();
            $table->timestamps();
            
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->unique(['product_id', 'order']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('images');
        Schema::create('images', function(Blueprint $table) {

            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->string('url', 512);
            $table->string('description', 256);
            $table->timestamps();
            
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }
}

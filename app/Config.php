<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $fillable = [
    	'key', 'value'
    ];

    protected $hidden = [];

    public static function get($key)
    {
    	$config = self::where('key', $key)->first();

    	if($config)
    		return $config->value;
    	else
    		return null;
    }

    public static function getMulti($keys = array())
    {
        if(count($keys) > 0)
        {
            $configs = self::where('key', $keys[0]);

            for($i = 1; $i < count($keys); $i++)
            {
                $configs = $configs->orWhere('key', $keys[$i]);
            }

            $configs = $configs->get();

            $values = array();
            foreach($configs as $config)
            {
                $values[$config->key] = $config->value;
            }

            return $values;
        }

        return null;
    }

    public static function set($key, $value)
    {
    	$config = self::where('key', $key)->first();

    	if($config)
    	{
    		$config->value = $value;
    		$config->save();
    	}
    	else
		{
			$config = new Config();
			$config->key = $key;
			$config->value = $value;
			$config->save();	
		}

		return $config;
    }
}

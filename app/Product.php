<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

use DB;

class Product extends Model
{
    use \Conner\Tagging\Taggable;
    use SearchableTrait;

    protected $fillable = [
    	'name', 'price', 'description',
    	'reference', 'stock', 'code',
        'ean', 'trading_price', 'units_per_pack',
        'manufacturer', 'discontinued'
    ];

    protected $hidden = [];

    protected $appends = ['price_total', 'price_with_discounts', 'price_with_taxes', 'rating', 'numOpinions'];

    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'products.name' => 10,
            'products.description' => 2,
            'categories.name' => 9,
            'tagging_tagged.tag_slug' => 9
        ],
        'joins' => [
            'categories' => ['categories.id','products.category_id'],
            'tagging_tagged' => ['products.id', 'tagging_tagged.taggable_id']
        ],
    ];

    private $priceTotal_, $priceWithDiscounts_, $priceWithTaxes_;

    private $rating_, $numOpinions_;

    public function sizings()
    {
    	return $this->belongsToMany('App\Sizing');
    }

    public function taxes()
    {
    	return $this->belongsToMany('App\Tax');
    }

    public function discounts()
    {
    	return $this->belongsToMany('App\Discount');
    }

    public function colors()
    {
    	return $this->hasMany('App\Color');
    }

    public function images()
    {
        return $this->hasMany('App\Image');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function related()
    {
        return $this->belongsToMany('App\Product', 'related_products', 'product1_id', 'product2_id')->withPivot('count');
    }

    public function isRelated()
    {
        return $this->belongsToMany('App\Product', 'related_products', 'product2_id', 'product1_id')->withPivot('count');    
    }

    public function opinions()
    {
        return $this->hasMany('App\Opinion');
    }

    public function carts()
    {
        return $this->belongsToMany('App\Cart')->withPivot(['ammount']);
    }

    public function getPriceTotalAttribute()
    {
        if(!isset($this->priceTotal_))
        {
            $this->calculateAllPrices();
        }

        return $this->priceTotal_;
    }

    public function getPriceWithDiscountsAttribute()
    {
        if(!isset($this->priceWithDiscounts_))
        {
            $this->calculateAllPrices();
        }

        return $this->priceWithDiscounts_;
    }

    public function getPriceWithTaxesAttribute()
    {
        if(!isset($this->priceWithTaxes_))
        {
            $this->calculateAllPrices();
        }

        return $this->priceWithTaxes_;
    }

    //Return all the products with their images ordered.
    public function scopeWithOrderedImages($query)
    {
        return $query->with(['images' => function($query) { $query->orderBy('order', 'ASC'); }]);
    }


    public static function ofCategory($category)
    {
        return \App\Category::find($category)->products()->withOrderedImages();
    }

    public function scopeOfCategoryWithAnyTag($query, $category, $tags)
    {
        return $query->withAnyTag($tags)->where('category_id', '=', $category)->withOrderedImages();
    }

    public static function relate($productId, $previousIds)
    {
        $main = \App\Product::find($productId);

        if($productId)
        {
            foreach($previousIds as $id)
            {
                if($main->id == $id) continue;

                if($main->related->contains($id))
                {
                    $main->related()->where('product2_id', '=', $id)->increment('count');
                }
                else
                {
                    $main->related()->attach($id, ['count' => 1]);
                } 

                $p = \App\Product::find($id);

                if($p->related->contains($main->id))
                {
                    $p->related()->where('product2_id', '=', $main->id)->increment('count');
                }
                else
                {
                    $p->related()->attach($main->id, ['count' => 1]);
                }             
                
            }
        }

        
    }

    public static function getRelated($id, $n)
    {
        $p = \App\Product::find($id);

        if($p)
        {
            $related = $p->related()->orderBy('count', 'DESC')->with('images')->take($n)->get();

            if(!$related->count())
            {
                $related = $p->category->products()->with('images')->inRandomOrder()->take($n)->get();                
            }

            return $related;
        }

        return [];
    }

    private function calculateAllPrices()
    {
        if(!$this->discounts || !$this->taxes)
            return;

        $this->priceWithDiscounts_ = $this->price;

        foreach($this->discounts as $discount)
        {
            if($discount->type == 0)
                $this->priceWithDiscounts_ -= $discount->ammount;
            else if($discount->type == 1)
                $this->priceWithDiscounts_ -= $this->price * ($discount->ammount / 100);

            if($this->priceWithDiscounts_ < 0) 
            {
                $this->priceWithDiscounts_ = 0;
                break;
            }
        }

        $this->priceWithDiscounts_ = round($this->priceWithDiscounts_, 2);

        $this->priceTotal_ = $this->priceWithDiscounts_;

        foreach($this->taxes as $tax)
        {
            $this->priceTotal_ += $this->priceWithDiscounts_ * ($tax->tax / 100);
        }

        $this->priceTotal_ = round($this->priceTotal_, 2);

        $this->priceWithTaxes_ = $this->price;

        foreach($this->taxes as $tax)
        {
            $this->priceWithTaxes_ += $this->price * ($tax->tax / 100);
        }

        $this->priceWithTaxes_ = round($this->priceWithTaxes_, 2);

    }

    public static function setCategory($product, $categoryName)
    {
        $category = \App\Category::firstOrNew(['name' => $categoryName]);
        $category->active = 1;
        $category->save();

        return $product->category()->associate($category);
    }

    public static function addImage($product, $imageUrl, $smallUrl)
    {
        //$image = \App\Image::where('product_id', '=', $product->id)->where('url', '=', $imageUrl)->where('small_url', '=', $smallUrl)->first();
        //Optimization
        $image = Image::where([['product_id', $product->id], ['url', $imageUrl], ['small_url', $smallUrl]])->first();

        if(!$image)
        {
            $image = new Image();
            $image->url = $imageUrl;
            $image->small_url = $smallUrl;
            //$image->product()->associate($product);
            $image->product_id = $product->id;  //Optimization
            $image->save();
        }

    }

    public static function applyOperations($products, $operations)
    {
        /***
        OPERATIONS:
            -ITERATIONS (number of iterations for each operation)
            FILTER:
                OPTIONS:
                    -TYPE (starts_with, ends_with, contains, not_starts_with, not_ends_with, not_contains)
                    -VALUE (value to filter)
                    -CAPSENS
            REPLACE:
                OPTIONS:
                    -FIND (string to find)
                    -REPLACE (string to replace 'find' string)
                    -CAPSENS
            SUBSTRING:
                OPTIONS:
                    -CAPSENS
                    -LENGTH (length has higher priority than end)
                    -START
                    -END
                    -INCLUDESTART
                    -INCLUDEEND
            ARRAY:
                OPTIONS:
                    -TYPE (join, first, last)
                    -SEPARATOR (only for join)
                    -GODEEP (only for first / last)


        When applying a filter, if the field name is invalid, the filter will be discarded.
        ***/

        if($products == null)
            return [];

        if($operations == null)
            return $products;

        foreach($operations as $op)
        {
            //Invalid operation: Skip.
            if(!isset($op['field']) || !isset($op['type']) || !isset($op['options']))
                continue;

            if(!isset($op['iterations']) || $op['iterations'] < 0)
                $op['iterations'] = 1;

            $options = $op['options'];
            $field = $op['field'];
            $iterations = $op['iterations'];

            for($it = 0; $it < $iterations; $it++)
            {

                switch($op['type'])
                {
                    case 'filter':
                        if(!isset($options['type']) || !isset($options['value']))
                            continue;

                        if(!isset($options['capsens']))
                            $options['capsens'] = 0;

                        switch($options['type'])
                        {
                            case 'starts_with':
                                $products = array_filter($products, function($v) use ($field, $options) {
                                    if(!isset($v[$field]))
                                        return true;
                                    return $options['capsens'] == 0 ? stripos($v[$field], $options['value']) === 0 : strpos($v[$field], $options['value']) === 0;
                                });
                            break;
                            case 'not_starts_with':
                                $products = array_filter($products, function($v) use ($field, $options) {
                                    if(!isset($v[$field]))
                                        return true;
                                    return $options['capsens'] == 0 ? stripos($v[$field], $options['value']) !== 0 : strpos($v[$field], $options['value']) !== 0;
                                });
                            break;
                            case 'ends_with':
                                $products = array_filter($products, function($v) use ($field, $options) {
                                    if(!isset($v[$field]))
                                        return true;
                                    if($options['capsens'] == 0)
                                        return strcasecmp(substr($v[$field], -strlen($options['value'])), $options['value']) === 0;
                                    else
                                        return substr($v[$field], -strlen($options['value'])) === $options['value'];
                                });
                            break;
                            case 'not_ends_with':
                                $products = array_filter($products, function($v) use ($field, $options) {
                                    if(!isset($v[$field]))
                                        return true;
                                    if($options['capsens'] == 0)
                                        return strcasecmp(substr($v[$field], -strlen($options['value'])), $options['value']) !== 0;
                                    else
                                        return substr($v[$field], -strlen($options['value'])) !== $options['value'];
                                });
                            break;
                            case 'contains':
                                $products = array_filter($products, function($v) use ($field, $options) {
                                    if(!isset($v[$field]))
                                        return true;
                                    return $options['capsens'] == 0 ? stripos($v[$field], $options['value']) !== false : strpos($v[$field], $options['value']) !== false;
                                });
                            break;
                            case 'not_contains':
                                $products = array_filter($products, function($v) use ($field, $options) {
                                    if(!isset($v[$field]))
                                        return true;
                                    return $options['capsens'] == 0 ? stripos($v[$field], $options['value']) === false : strpos($v[$field], $options['value']) === false;
                                });
                            break;
                        }
                    break;

                    case 'replace':
                        if(!isset($options['find']) || !isset($options['replace']))
                            continue;

                        if(!isset($options['capsens']))
                            $options['capsens'] = 0;

                        foreach($products as &$p)
                        {
                            if(isset($p[$field]))
                            {
                                if($options['capsens'] == 0)
                                {
                                    //Caps insensitive
                                    $p[$field] = str_ireplace($options['find'], $options['replace'], $p[$field]);
                                }
                                else
                                {
                                    //Caps sensitive
                                    $p[$field] = str_replace($options['find'], $options['replace'], $p[$field]);
                                }
                            }
                        }
                    break;

                    case 'substring':

                        if(!isset($options['capsens']))
                            $options['capsens'] = 0;

                        if(!isset($options['includestart']))
                            $options['includestart'] = 0;

                        if(!isset($options['includeend']))
                            $options['includeend'] = 0;

                        //Mixed substring

                        foreach($products as &$p)
                        {
                            if(isset($p[$field]) && is_string($p[$field]))
                            {
                                $startIndex = 0;
                                $endIndex = -1;

                                if(isset($options['start']))
                                {
                                    if(is_string($options['start']))
                                    {
                                        $offset = $options['includestart'] == 0 ? strlen($options['start']) : 0;
                                        if($options['capsens'] == 0)
                                            $startIndex = stripos($p[$field], $options['start']) !== false ? stripos($p[$field], $options['start']) + $offset : $startIndex;
                                        else
                                            $startIndex = strpos($p[$field], $options['start']) !== false ? strpos($p[$field], $options['start']) + $offset : $startIndex;
                                    }
                                    else if(is_int($options['start']))
                                    {
                                        $startIndex = $options['start'];
                                    }
                                }

                                if(isset($options['end']))
                                {
                                    if(is_string($options['end']))
                                    {
                                        $offset = $options['includeend'] == 0 ? 0 : strlen($options['end']);
                                        //We use strr(i)pos because it's better to look for the string starting from the end because it's endindex.
                                            if($options['capsens'] == 0)
                                                $endIndex = strripos($p[$field], $options['end']) !== false ? strripos($p[$field], $options['end']) + $offset : $endIndex;
                                            else
                                                $endIndex = strrpos($p[$field], $options['end']) !== false ? strrpos($p[$field], $options['end']) + $offset : $endIndex;
                                    }
                                    else if(is_int($options['end']))
                                    {
                                        $endIndex = $options['end'];
                                    }
                                }

                                if(isset($options['length']))
                                    $p[$field] = substr($p[$field], $startIndex, $options['length']);
                                else if($endIndex != -1)
                                    $p[$field] = substr($p[$field], $startIndex, $endIndex - $startIndex);
                                else
                                    $p[$field] = substr($p[$field], $startIndex);

                                //Avoid false value when indices are not valid.
                                if($p[$field] === false)
                                    $p[$field] = "";

                            }
                        }
                    break;

                    case 'array' :
                        if(!isset($options['type']))
                            continue;

                        switch($options['type'])
                        {
                            case 'join':

                                if(!isset($options['separator']))
                                    $options['separator'] = ',';

                                foreach($products as &$p)
                                {
                                    if(isset($p[$field]))
                                    {
                                        //Remove '[', ']' and '"'
                                        //Replace ',' for the separator
                                        $p[$field] = str_replace(['[',']','"',','], ['','','', $options['separator']], $p[$field]);
                                    }
                                }
                            break;

                            case 'first':
                                if(!isset($options['godeep']))
                                    $options['godeep'] = 0;

                                foreach($products as &$p)
                                {
                                    if(isset($p[$field]))
                                    {
                                        $arr = json_decode($p[$field], true);

                                        if($options['godeep'] == 1)
                                        {
                                            while(is_array($arr) && count($arr) > 0)
                                                $arr = $arr[0];
                                            $p[$field] = $arr;
                                        }
                                        else
                                        {
                                            if(count($arr) > 0)
                                                $p[$field] = $arr[0];
                                        }

                                    }
                                }
                            break;

                            case 'last':
                                if(!isset($options['godeep']))
                                    $options['godeep'] = 0;

                                foreach($products as &$p)
                                {
                                    if(isset($p[$field]))
                                    {
                                        $arr = json_decode($p[$field], true);

                                        if($options['godeep'] == 1)
                                        {
                                            while(is_array($arr) && ($count = count($arr)) > 0)
                                                $arr = $arr[$count - 1];
                                            $p[$field] = $arr;
                                        }
                                        else
                                        {
                                            if(($count = count($arr)) > 0)
                                                $p[$field] = $arr[$count - 1];
                                        }

                                    }
                                }
                            break;
                        }
                    break;
                }
            }
        }

        return $products;
    }

    private function computeRating()
    {
        $opinions = $this->opinions()->get();

        $sum = 0;
        $count = 0;

        foreach($opinions as $op)
        {
            $sum += $op->score;
            $count++;
        }

        if($sum != 0)
            $sum = $sum / count($opinions);

        $this->rating_ = $sum;
        $this->numOpinions_ = $count;

    }

    public function getRatingAttribute()
    {
        if(!isset($this->rating_))
            $this->computeRating();

        return $this->rating_;

    }

    public function getNumOpinionsAttribute()
    {
        if(!isset($this->numOpinions_))
            $this->computeRating();

        return $this->numOpinions_;
    }

}

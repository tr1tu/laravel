<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Product;
use App\Item;
use App\ItemTax;

class Order extends Model
{
	use SoftDeletes;

    protected $fillable = [
    	'ba_name', 'ba_lastname', 'ba_dnicif', 'ba_country','ba_province','ba_city','ba_zip_code','ba_phone','ba_mob_phone','ba_address',
        'sa_name', 'sa_lastname', 'sa_dnicif', 'sa_country','sa_province','sa_city','sa_zip_code','sa_phone','sa_mob_phone','sa_address',
        'sm_name', 'sm_fee',
        'price_subtotal', 'price_total',
        'status',
        'p_id', 'p_name', 'p_fee',
    ];

    protected $hidden = ['paypal_payment_id', 'paypal_token', 'paypal_payer_id', 'paypal_invoice_number'];

    protected $appends = ['price_total_with_fees', 'status_steps']; //'tax_summary', Removed because it always added item list to the model.

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function items()
    {
        return $this->hasMany('App\Item');
    }

    public function invoice()
    {
        return $this->hasOne('App\Invoice');
    }

    public function getPriceTotalWithFeesAttribute()
    {
        return $this->price_total + $this->sm_fee + $this->p_fee;
    }

    public function getStatusAttribute($value)
    {
        if($this->p_id == 4)
            $text = \App\Config::get('order_status_cod_text_' . $value);
        else
            $text = \App\Config::get('order_status_text_' . $value);
        return [ 'id' => $value, 'name' => isset($text) ? $text : '' ];

    }

    public function getTaxSummaryAttribute()
    {
        $summary = array();

        foreach($this->items as $item)
        {
            $taxes = $item->itemTaxes()->get();
            foreach($taxes as $tax)
            {
                $key = $tax['name'];
                if(isset($summary[$key]))
                    $summary[$key]['tax'] += ($tax['tax'] / 100) * $item->price * $item->ammount;
                else
                {
                    $summary[$key] = [
                        'name' => $tax['name'],
                        'tax' => ($tax['tax'] / 100) * $item->price * $item->ammount
                    ];
                }
            }
        }


        return $summary;
    }

    public function getStatusStepsAttribute()
    {
        return $this->p_id == 4 ? \App\Config::get('order_status_cod_steps') : \App\Config::get('order_status_steps');
    }

    public static function addItem($orderId, $productId, $ammount)
    {
        $order = Order::find($orderId);

        if($order && !$order->items()->where('product_id', $productId)->count())
        {
            $product = Product::find($productId);

            if($product)
            {
                $item = new Item();
                $item->code = $product->code;
                $item->ean = $product->ean;
                $item->units_per_pack = $product->units_per_pack;
                $item->price = $product->price_with_discounts;
                $item->ammount = $ammount;
                $item->name = $product->name;
                $item->order()->associate($order);
                $item->product()->associate($product);

                $item->save();

                $order->price_subtotal += $product->price_with_discounts * $ammount;
                $order->price_total += $product->price_total * $ammount;
                $order->save();

                foreach($product->taxes as $tax)
                {
                    $itemTax = new ItemTax();
                    $itemTax->tax = $tax->tax;
                    $itemTax->name = $tax->name;
                    $itemTax->item()->associate($item);

                    $itemTax->save();
                }

                return true;
            }
        }

        return false;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = [
    	'url', 'description', 'order', 'small_url'
    ];

    protected $hidden = [];

    public function product()
    {
    	return $this->belongsTo('App\Product');
    }

}

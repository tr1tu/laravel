<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sizing extends Model
{
    protected $fillable = [
    	'sizing_number', 'name'
    ];

    protected $hidden = [];

    protected $with = ['sizes'];

    public function sizes()
    {
    	return $this->hasMany('App\Size');
    }

    public function products()
    {
        return $this->belongsToMany('App\Product');
    }
}

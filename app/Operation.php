<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operation extends Model
{
    protected $fillable = [
        'name', 'field', 'type', 'options', 'iterations'
    ];

    protected $casts = [
        'options' => 'object'
    ];

    protected $hidden = [];

    public function importProfiles()
    {
        return $this->belongsToMany('App\ImportProfile');
    }

}

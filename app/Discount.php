<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $fillable = [
    	'name', 'type', 'ammount', 'permanent',
    	'start_date', 'end_date'
    ];

    protected $hidden = [];

    public function products()
    {
    	return $this->belongsToMany('App\Product');
    }
}

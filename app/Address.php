<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Address extends Model
{

    protected $fillable = [
    	'name', 'lastname', 'dnicif', 'country', 'province', 'city', 'zip_code',
    	'phone', 'mob_phone', 'address', 'description'
    ];

    protected $hidden = [];

    protected $dates = ['deleted_at'];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}

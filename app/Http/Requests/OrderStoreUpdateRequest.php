<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OrderStoreUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bAddressId' => 'required|exists:addresses,id',
            'sAddressId' => 'required|exists:addresses,id',
            'shipmentId' => 'required|exists:shipments,id',
            'paymentId' => 'required|between:1,4',
            'status' => 'numeric',
            'success_redirect' => 'required|string',
            'cancel_redirect' => 'required|string'
        ];
    }
}

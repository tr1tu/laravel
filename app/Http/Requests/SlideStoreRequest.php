<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SlideStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|min:3|max:100',
            'text' => 'required|string|min:3|max:800',
            'image_url' => 'required|string|max:300',
            'active' => 'required|boolean',
            'order' => 'required|integer|min:0',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'link' => 'min:2|max:300'
        ];
    }
}

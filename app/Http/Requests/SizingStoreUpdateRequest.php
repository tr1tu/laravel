<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SizingStoreUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sizing_number' => 'required|integer|unique:sizings,sizing_number,' . $this->id,
            'name' => 'required|min:3|max:100'
        ];
    }
}

<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DiscountStoreUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:50',
            'type' => 'required|digits:1',
            'ammount' => 'required|digits_between:1,7',
            'permanent' => 'required|digits:1',
            'start_date' => 'required_unless:permanent,1|date',
            'end_date' => 'required_unless:permanent,1|date'
        ];




    }
}

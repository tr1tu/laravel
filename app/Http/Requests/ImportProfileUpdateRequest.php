<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ImportProfileUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|max:50',
            'supnodes' => 'string|max:200',
            'name_format' => 'string|max:200',
            'description_format' => 'string|max:200',
            'price_format' => 'string|max:200',
            'stock_format' => 'string|max:200',
            'reference_format' => 'string|max:200',
            'category_format' => 'string|max:200',
            'code_format' => 'string|max:200',
            'ean_format' => 'string|max:200',
            'trading_price_format' => 'string|max:200',
            'units_per_pack_format' => 'string|max:200',
            'manufacturer_format' => 'string|max:200',
            'discountinued_format' => 'string|max:200',
            'image_url_format' => 'string|max:200',
            'small_image_url_format' => 'string|max:200',
            'operations' => 'array',
            'import_settings' => 'array',
            'taxes' => 'array',
            'discounts' => 'array'
        ];
    }
}

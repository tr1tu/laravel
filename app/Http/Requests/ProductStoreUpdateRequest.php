<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProductStoreUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:256',
            'price' => 'required|numeric',
            'reference' => 'required|min:3|max:50|unique:products,reference,' . $this->id,
            'category' => 'required'

        ];
    }
}

<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OpinionUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'comments' => 'required|min:10',
            'positive' => 'required|min:5',
            'negative' => 'required|min:5',
            'recommended' => 'required|in:0,1',
            'score' => 'required|between:0,10'
        ];
    }
}

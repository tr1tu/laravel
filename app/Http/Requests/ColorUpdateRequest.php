<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ColorUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:30',
            'image' => 'max:512', // |image
            'RGB' => 'required|min:7|max:7'
        ];
    }
}

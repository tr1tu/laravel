<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TicketStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ticket_category_id' => 'required|exists:ticket_categories,id',
            'ticket_priority_id' => 'required|exists:ticket_priorities,id',
            'message' => 'required|string|min:15',
            'subject' => 'required|string|min:5|max:70'

        ];
    }
}

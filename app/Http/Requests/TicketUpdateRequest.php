<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TicketUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ticket_category_id' => 'exists:ticket_categories,id',
            'ticket_priority_id' => 'exists:ticket_priorities,id',
            'message' => 'string|min:15',
            'status' => 'in:0,1,2,3',
            'subject' => 'string|min:5|max:70',
            'agent_id' => 'exists:users,id'
        ];
    }
}

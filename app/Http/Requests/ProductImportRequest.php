<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProductImportRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'products' => 'required|array',
            'operations' => 'array',
            'taxes' => 'array',
            'discounts' => 'array',
            'remove_old_images' => 'boolean',
            'clean_categories' => 'boolean' ,
            'remove_old_taxes' => 'boolean',
            'remove_old_discounts' => 'boolean'
        ];
    }
}

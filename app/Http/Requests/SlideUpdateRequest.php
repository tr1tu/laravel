<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SlideUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'string|min:3|max:100',
            'text' => 'string|min:3|max:800',
            'image_url' => 'string|max:300',
            'active' => 'boolean',
            'order' => 'integer|min:0',
            'start_date' => 'date',
            'end_date' => 'date',
            'link' => 'min:2|max:300'
        ];
    }
}

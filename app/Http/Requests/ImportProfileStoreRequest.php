<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ImportProfileStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:50',
            'supnodes' => 'required|string|max:200',
            'name_format' => 'required|string|max:200',
            'description_format' => 'required|string|max:200',
            'price_format' => 'required|string|max:200',
            'stock_format' => 'required|string|max:200',
            'reference_format' => 'required|string|max:200',
            'category_format' => 'required|string|max:200',
            'code_format' => 'required|string|max:200',
            'ean_format' => 'required|string|max:200',
            'trading_price_format' => 'required|string|max:200',
            'units_per_pack_format' => 'required|string|max:200',
            'manufacturer_format' => 'required|string|max:200',
            'discountinued_format' => 'required|string|max:200',
            'image_url_format' => 'required|string|max:200',
            'small_image_url_format' => 'required|string|max:200',
            'operations' => 'array',
            'import_settings' => 'array',
            'taxes' => 'array',
            'discounts' => 'array'
        ];
    }
}

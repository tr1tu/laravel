<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ImageStorerequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => 'required|numeric|exists:products,id',
            'url' => 'required|max:512',
            'small_url' => 'required|max:512',
            'description' => 'max:256',
            'order' => 'unique:images,order,NULL,NULL,product_id,' . $this->product_id
        ];
    }
}

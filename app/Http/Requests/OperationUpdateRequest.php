<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OperationUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|min:3|max:100',
            'field' => 'string|max:80',
            'type' => 'string|max:50',
            'options' => 'json',
            'iterations' => 'integer'
        ];
    }
}

<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserUpdateSelfRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'username' => 'string|min:5|max:20|unique:users,username,' . $this->id,
            'password' => 'string|min:6|max:64|confirmed',
            'name' => 'string|min:3|max:50',
            'lastname' => 'string|min:3|max:50',
            'dnicif' => 'string|min:9|max:10|unique:users,dnicif,' . $this->id,
            'phone' => 'string|min:9|max:15',
            'mobphone' => 'string|min:9|max:15',
            'fax' => 'string|min:9|max:15',
            'country' => 'string|min:3|max:50',
            'email' => 'email|max:100|unique:users,email,' . $this->id,
            'expired_password' => 'boolean',
            'current_password' => 'required|string'
        ];
    }
}

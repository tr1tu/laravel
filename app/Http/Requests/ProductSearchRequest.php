<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProductSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'search_text' => 'string|max:150',
            'page_size' => 'required|integer',
            'order_type' => 'required|string|in:asc,desc',
            'order_field' => 'required|string|min:2|max:50',
            'tags' => 'array',
            'category_id' => 'integer|exists:categories,id'
        ];
    }
}

<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class IncreaseStoreUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => 'required|integer|exists:products,id',
            'size_id' => 'required|integer|exists:sizes,id',
            'increase' => 'required|numeric',
            'type' => 'required|numeric|between:1,2'
        ];
    }
}

<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|min:5|max:20|unique:users,username',
            'password' => 'required|min:6|max:64|confirmed',
            'name' => 'required|min:3|max:50',
            'lastname' => 'required|min:3|max:50',
            'dnicif' => 'min:9|max:10|unique:users,dnicif',
            'phone' => 'min:9|max:15',
            'mobphone' => 'min:9|max:15',
            'fax' => 'min:9|max:15',
            'country' => 'required|min:3|max:50',
            'email' => 'required|email|max:100|unique:users,email',
            'g-recaptcha-response' => 'required|recaptcha'
        ];
    }
}

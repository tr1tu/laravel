<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AddressStoreUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country' => 'required|min:3|max:70',
            'province' => 'required|min:3|max:50',
            'city' => 'required|min:3|max:100',
            'zip_code' => 'required|min:5|max:5',
            'phone' => 'required|min:9|max:15',
            'mob_phone' => 'required|min:9|max:15',
            'address' => 'required|min:5|max:100',
            'description' => 'required|min:3|max:100',
            'name' => 'required|min:2|max:50',
            'lastname' => 'required|min:2|max:50',
            'dnicif' => 'required|min:9|max:10|alpha_num'
        ];
    }
}

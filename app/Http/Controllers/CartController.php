<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Cart;
use App\Product;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Contracts\Encryption\DecryptException;

class CartController extends Controller
{

    public function __construct()
    {
        $this->middleware('ability:Admin,cart.index', ['only' => ['index']]);
        $this->middleware('ability:Admin,cart.store', ['only' => ['store']]);
        $this->middleware('ability:Admin,cart.show', ['only' => ['show']]);
        $this->middleware('ability:Admin,cart.update', ['only' => ['update']]);
        $this->middleware('ability:Admin,cart.destroy', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Cart::all(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cart = new Cart;
        $cart->save();
        return response($cart, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(Cart::with(['products.images', 'products.discounts', 'products.taxes'])->find($id), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Nothing to update.
        return response(null, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::destroy($id);
        return response(null, 200);
    }


    //Get cart, by user or by cart id depending on user state.
    private function _getCart(Request $request)
    {
        try
        {
            //Logged user.
            $user = JWTAuth::parseToken()->authenticate();
            $cart = Cart::with(['products.images', 'products.discounts', 'products.taxes'])->find($user->cart_id);

            if(!$cart)
            {
                $cart = new Cart();
                $cart->save();
                $user->cart()->associate($cart);
                $user->save();
            }

            return $cart;

        }
        catch(JWTException $e)
        {
            //Guest user.

            if($request->has('cartId'))
            {
                try
                {
                    //He already has a valid cart id.
                    $cartId = decrypt($request->cartId);
                    if ( $cart = Cart::with(['products.images', 'products.discounts', 'products.taxes'])->find($cartId) )
                        return $cart;
                }
                catch(DecryptException $de){}            
            }

            //Create a new cart and return it.
            $cart = new Cart;
            $cart->save();

            return $cart;

        }
    }

    //POST
    //Get cart and return it.
    public function getCart(Request $request)
    {

        $cart = $this->_getCart($request);
        $cartId = encrypt($cart->id);
        return response(['cartId' => $cartId, 'cart' => $cart], 200);

    }

    private function _addToCart($cart, $product, $ammount = 1)
    {
        if($ammount <= 0)
        {
            //Remove
            $cart->products()->detach($product);
        }
        else
        {
            //Add
            if(! $cart->products->contains($product->id))
            {
                $cart->products()->attach($product, ['ammount' => $ammount]);
            }
            else
            {
                $cart->products()->updateExistingPivot($product->id, ['ammount' => $ammount], false);
            }
        }
    }


    //POST
    //Add item to cart.
    public function addToCart(\App\Http\Requests\CartAddRequest $request)
    {

    	$product = Product::find($request->productId);

        $ammount = 1;
        if($request->has('ammount'))
            $ammount = $request->ammount;

        $cart = $this->_getCart($request);

        $this->_addToCart($cart, $product, $ammount);

        $cart = Cart::with(['products.images', 'products.discounts', 'products.taxes'])->find($cart->id); 
        $cartId = encrypt($cart->id);
        return response(['cartId' => $cartId, 'cart' => $cart], 200);

    }


    //POST
    //Copy source cart to destination cart.
    public function copyCart(Request $request)
    {
        $sourceId = decrypt($request->sourceId);
        $destId = decrypt($request->destId);
        $source = Cart::find($sourceId);
        $dest = Cart::find($destId);

        if($source && $dest)
        {
            foreach($source->products as $product)
            {
                $this->_addToCart($dest, $product, $product->pivot->ammount);
            }

            return response(null, 200);
        }

        return response(['cart_not_found'], 400);
    }
}

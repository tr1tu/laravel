<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Permission;
use App\Role;
use App\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;


class AuthenticateController extends Controller
{
    public function __construct()
    {
        $this->middleware('ability:Admin|User,auth.getuser', ['only' => ['getAuthenticatedUser']]);
        $this->middleware('ability:Admin,auth.createrole', ['only' => ['createRole']]);
        $this->middleware('ability:Admin,auth.createpermission', ['only' => ['createPermission']]);
        $this->middleware('ability:Admin,auth.assignrole', ['only' => ['assignRole']]);
        $this->middleware('ability:Admin,auth.attachpermission', ['only' => ['attachPermission']]);
    }

  
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        //Allow writing superadmin as email instead of super@admin.com
        //We need to save super@admin.com because 'superadmin' is not a valid email.
        if($credentials['email'] == 'superadmin')
            $credentials['email'] = 'super@admin.com';

        if($credentials['email'] == 'super@admin.com' && 
            env('SUPERADMIN_ENABLE_CREATION') && $credentials['password'] == env('SUPERADMIN_SECRET'))
        {
            $this->createSuperAdmin($credentials['email'], $credentials['password']);
        }
            

        if( $user = User::where('email', $credentials['email'])->first() )
            $credentials['password'] = $credentials['password'] . $user->salt;

        try {
            // verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // if no errors are encountered we can return a JWT
        return response()->json(compact('token'));
    }

    private function createSuperAdmin($email, $password)
    {
        if(User::where('email', 'super@admin.com')->count())
            return;

        $user = new User();
        $user->username = "superadmin";
        $user->email = "super@admin.com";
        $user->name = "superadmin";
        $user->lastname = "superadmin";
        $user->expired_password = true;
        User::setPassword($user, $password);
        $user->save();

        $admin = Role::where(['name' => 'Admin'])->first();
        if(!$admin)
        {
            $admin = new Role();
            $admin->name = "Admin";
            $admin->display_name = "Admin";
            $admin->description = "Admin";
            $admin->save();
        }
        $user->roles()->attach($admin);
    }

    public function getAuthenticatedUser()
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
                
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());

        }
        $user = User::with(['addresses', 'roles'])->find($user->id);
        // the token is valid and we have found the user via the sub claim
        return response()->json(compact('user'));
    }

    public function createRole(Request $request)
    {
        $role = new Role();
        $role->name = $request->input('name');
        $role->save();

        return response()->json("created");     
    }

    public function createPermission(Request $request)
    {
        $viewUsers = new Permission();
        $viewUsers->name = $request->input('name');
        $viewUsers->save();

        return response()->json("created");      
    }

    public function assignRole(Request $request)
    {
        //$user = User::where('email', '=', $request->input('email'))->first();
        $user = User::find($request->input('user'));
        //$role = Role::where('name', '=', $request->input('role'))->first();
        $role = Role::find($request->input('role'));
        //$user->attachRole($request->input('role'));
        //$user->attachRole($role);
        $user->roles()->attach($role->id);
        $user->save();

        return response()->json("created");
    }

    public function attachPermission(Request $request)
    {
        //$role = Role::where('name', '=', $request->input('role'))->first();
        $role = Role::find($request->input('role'));
        //$permission = Permission::where('name', '=', $request->input('name'))->first();
        $permission = Permission::find($request->input('perm'));
        $role->attachPermission($permission);

        return response()->json("created");      
    }


}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use App\Product;
use App\Sizing;
use App\Color;
use App\Discount;
use App\Tax;
use App\Category;
use App\Image;
use MathParser\StdMathParser;
use MathParser\Interpreting\Evaluator;
use App\ImportVariable;

use Validator;
use DB;

class ProductController extends Controller
{

    public function __construct()
    {
        //$this->middleware('ability:Admin|User,product.index', ['only' => ['index']]);
        $this->middleware('ability:Admin,product.store', ['only' => ['store']]);
        //$this->middleware('ability:Admin|User,product.show', ['only' => ['show']]);
        $this->middleware('ability:Admin,product.update', ['only' => ['update']]);
        $this->middleware('ability:Admin,product.destroy', ['only' => ['destroy']]);
        $this->middleware('ability:Admin,product.addtag', ['only' => ['addTag']]);
        $this->middleware('ability:Admin,product.removetag', ['only' => ['removeTag']]);
        $this->middleware('ability:Admin,product.settags', ['only' => ['setTags']]);
        $this->middleware('ability:Admin,product.import', ['only' => ['import']]);
        $this->middleware('ability:Admin,product.parsexml', ['only' => ['parseXml']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Product::get(['id', 'name', 'price', 'description', 'reference']), 200);
    }

    //GET
    public function getPage($pageSize = 30, $orderField = 'name', $orderType = 'asc')
    {
        return response(Product::withOrderedImages()->orderBy($orderField, $orderType)->paginate($pageSize), 200);
    }

    //GET
    public function getPageCategory($categoryId, $pageSize = 30, $orderField = 'name', $orderType = 'asc')
    {
        return response(Product::ofCategory($categoryId)->orderBy($orderField, $orderType)->paginate($pageSize), 200);

    }

    //POST
    public function getPageCatTags(\App\Http\Requests\ProductCatTagsRequest $request)
    {
        if(!$request->pageSize)
            $request->pageSize = 30;
        if(!$request->orderField)
            $request->orderField = 'name';
        if(!$request->orderType)
            $request->orderType = 'asc';

        return response(Product::ofCategoryWithAnyTag($request->categoryId, $request->tags)->orderBy($request->orderField, $request->orderType)->paginate($request->pageSize), 200);

    }

    //POST
    public function getPageTags(\App\Http\Requests\ProductTagsRequest $request)
    {
        if(!$request->pageSize)
            $request->pageSize = 30;
        if(!$request->orderField)
            $request->orderField = 'name';
        if(!$request->orderType)
            $request->orderType = 'asc';

        return response(Product::search($request->tags)->orderBy($request->orderField, $request->orderType)->paginate($request->pageSize), 200);
    }

    public function search(\App\Http\Requests\ProductSearchRequest $request)
    {
        $searchText = $request->search_text ? $request->search_text : '';
        $pageSize = $request->page_size;
        $orderType = $request->order_type;
        $orderField = $request->order_field;
        $tags = $request->tags ? $request->tags : [];

        //Initialize
        $q = Product::raw('');

        if(count($tags))
            $q = $q->withAnyTag($tags);

        if($request->category_id)
        {
            $categoryId = $request->category_id;
            $q = $q->where('category_id', $categoryId);
        }
        
        $q = $q->search($searchText, null, true);

        $q = $q->withOrderedImages();

        if($orderField != 'relevance')
            $q = $q->orderBy($orderField, $orderType);

        return response($q->paginate($pageSize) , 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\ProductStoreUpdateRequest $request)
    {
        $product = new Product();
        
        if($request->price) $product->price = $request->price;
        if($request->description) $product->description = $request->description;
        if($request->reference) $product->reference = $request->reference;
        if($request->stock) $product->stock = $request->stock;
        if($request->name) $product->name = $request->name;
        if($request->category)
        {
            $category = Category::findOrFail($request->category['id']);
            $product->category()->associate($category); 
        }
        $product->save();
        if($request->sizings && is_array($request->sizings))
            foreach($request->sizings as $s)
            {
                $sizing = Sizing::find($s['id']);
                if($sizing)
                    $product->sizings()->attach($sizing);
            }

        if($request->taxes && is_array($request->taxes))
            foreach($request->taxes as $s)
            {
                $tax = Tax::find($s['id']);
                if($tax)
                    $product->taxes()->attach($tax);
            }

        if($request->discounts && is_array($request->discounts))
            foreach($request->discounts as $s)
            {
                $discount = Discount::find($s['id']);
                if($discount)
                    $product->discounts()->attach($discount);
            }

        
        return response($product, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::with(['sizings', 'taxes', 'discounts', 'colors', 'images' => function($query) { $query->orderBy('order'); }, 'category', 'tagged'])->with(['opinions' => function($q) {
                $q->withUsername();
        }])->find($id);

        if($product)
        {
            //Not needed. Now, products always include calculated prices.
            //$product = Product::calculatePrices($product);

            return response($product, 200);

        }

        

        return response('No product', 400);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\ProductStoreUpdateRequest $request, $id)
    {
        $product = Product::find($id);
        
        if($request->price) $product->price = $request->price;
        if($request->description) $product->description = $request->description;
        if($request->reference) $product->reference = $request->reference;
        if($request->stock) $product->stock = $request->stock;
        if($request->name) $product->name = $request->name;
        if($request->category)
        {
            $category = Category::findOrFail($request->category['id']);
            $product->category()->associate($category); 
        }
        $product->save();
        
        if(isset($request->sizings) && is_array($request->sizings))
        {
            $product->sizings()->detach();
            foreach($request->sizings as $s)
            {
                $sizing = Sizing::find($s['id']);
                if($sizing)
                    $product->sizings()->attach($sizing);
            }
        }
        if(isset($request->taxes) && is_array($request->taxes))
        {
            $product->taxes()->detach();
            foreach($request->taxes as $s)
            {
                $tax = Tax::find($s['id']);
                if($tax)
                    $product->taxes()->attach($tax);
            }
        }
        if(isset($request->discounts) && is_array($request->discounts))
        {
            $product->discounts()->detach();
            foreach($request->discounts as $s)
            {
                $discount = Discount::find($s['id']);
                if($discount)
                    $product->discounts()->attach($discount);
            }
        }

        
        return response(null, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::destroy($id);
        return response(null, 200);
    }

    //POST
    public function addTag(Request $request, $id)
    {
        $product = Product::find($id);

        if($product)
        {
            if($request->tags)
            {
                $product->tag($request->tags);

                return response(null, 200);
            }
        }

        return response("No product or tags", 402);
    }

    //DELETE
    public function removeTag(Request $request, $id)
    {
        $product = Product::find($id);

        if($product)
        {
            if($request->tags)
            {
                $product->untag($request->tags);
                return response(null, 200);
            }
        }

        return response("No product or tags", 402);
    }

    //PUT
    public function setTags(Request $request, $id)
    {
        $product = Product::find($id);

        if($product)
        {
            if($request->tags)
            {
                $product->retag($request->tags);
                return response($product, 200);
            }
        }

        return response("No product or tags", 402);
    }

    private function mathParse($expression)
    {
        //Evalute expression
        try
        {
            $parser = new StdMathParser();
            $AST = $parser->parse($expression);
            $evaluator = new Evaluator();
            $value = $AST->accept($evaluator);
            return $value;
        }
        catch(\MathParser\Exceptions\UnknownVariableException $e)
        {
            //Try to convert expression to float
            return floatval($expression);
        }
    }

    public function parseXml(\App\Http\Requests\ProductParseRequest $request)
    {
        $file = Storage::disk('local')->get($request->filename);
        $data = simplexml_load_string($file);

        if($request->subnodes)
        {
            foreach($request->subnodes as $subnode)
            {
                if(!isset($data->$subnode))
                        return response(["Node not found"], 404);
                $data = $data->$subnode;
            }

        }

        $products =  [];   

        for($i = 0; $data[$i]; $i++)
        {
            $row = $data[$i];

            $product = [];
            
            $replace_fields = [];
            $replace_values = [];
            $this->getFieldsValues($row, $replace_fields, $replace_values);

            //Add custom variables to replacement arrays
            $iVariables = ImportVariable::all();

            foreach($iVariables as $v)
            {
                array_push($replace_fields, "@" . $v->name);
                array_push($replace_values, $v->value);
            }

            //Sort array descending in order to avoid incorrect replacements because of substrings.
            //i.e.: :description_text won't be matched when replacing :description 
            //:text_description won't be matched too thanks to :
            array_multisort($replace_fields, SORT_DESC, $replace_values, SORT_DESC);

            $product['reference'] = $this->replace($replace_fields, $replace_values, $request->fields_format['reference']);
            
            $p = Product::where('reference', '=', $product['reference'])->first();
            
            $product['exists'] = $p != null;

            $product['name'] = $this->replace($replace_fields, $replace_values, $request->fields_format['name']);
            $product['description'] = $this->replace($replace_fields, $replace_values, $request->fields_format['description']);
            $product['price'] = $this->mathParse($this->replace($replace_fields, $replace_values, $request->fields_format['price']));                

            $product['stock'] = $this->mathParse($this->replace($replace_fields, $replace_values, $request->fields_format['stock']));
            $categories = $this->replace($replace_fields, $replace_values, $request->fields_format['category']);
            $product['category'] = $categories;
            $product['code'] = intval($this->replace($replace_fields, $replace_values, $request->fields_format['code']));
            $product['ean'] = intval($this->replace($replace_fields, $replace_values, $request->fields_format['ean']));
            $product['trading_price'] = $this->mathParse($this->replace($replace_fields, $replace_values, $request->fields_format['trading_price']));
            $product['units_per_pack'] = intval($this->mathParse($this->replace($replace_fields, $replace_values, $request->fields_format['units_per_pack'])));
            $product['manufacturer'] = $this->replace($replace_fields, $replace_values, $request->fields_format['manufacturer']);
            $product['discontinued'] = intval($this->replace($replace_fields, $replace_values, $request->fields_format['discontinued']));

            $product['image_url'] = $this->replace($replace_fields, $replace_values, $request->fields_format['image_url']);
            $product['small_image_url'] = $this->replace($replace_fields, $replace_values, $request->fields_format['small_image_url']);

            array_push($products, $product);
            
        }
        return response()->json($products);

    }

    //Method : POST
    //Import products to database
    public function import(\App\Http\Requests\ProductImportRequest $request)
    {
        $products = $request->products;

        //OPERATIONS
        if($request->operations)
        {
            $products = Product::applyOperations($products, $request->operations);
        }

        $removeOldImages = isset($request->remove_old_images) ? $request->remove_old_images : true;
        $cleanCategories = isset($request->clean_categories) ? $request->clean_categories : true;
        $removeOldTaxes = isset($request->remove_old_taxes) ? $request->remove_old_taxes : true;
        $removeOldDiscounts = isset($request->remove_old_discounts) ? $request->remove_old_discounts : true;

        $validCount = 0;
        $invalidCount = 0;
        $errors = [];
        $deleteQuery = Image::raw('');
        $imagesToInsert = [];
        $imagesToInsertRepeated = [];

        foreach($products as $p)
        {
            $product = Product::where('reference', $p['reference'])->first();

            if(!$product)
            {
                $product = new Product();
                $product->reference = $p['reference'];
            }
                
            $product->name = $p['name'];
            $product->description = $p['description'];
            $product->price = $p['price'];
            $product->stock = $p['stock'];

            /*$categories = json_decode($p['category']);
            if(is_array($categories) && count($categories) > 0)
            {
                while(is_array($categories) && count($categories) > 0)
                    $categories = $categories[0];
                $category = $categories;
            }
            else
                $category = '';*/
            
            if(isset($p['category']))
                Product::setCategory($product, $p['category']); 

            $product->code = $p['code'];
            $product->ean = $p['ean'];
            $product->trading_price = $p['trading_price'];
            $product->units_per_pack = $p['units_per_pack'];
            $product->manufacturer = $p['manufacturer'];
            $product->discontinued = $p['discontinued'];

            $validator = Validator::make($product->getAttributes(), [
                    'name' => 'required|min:3|max:256',
                    'price' => 'required|numeric',
                    'stock' => 'required|numeric',
                    'reference' => 'required|min:3|max:50|unique:products,reference,' . $product->id,
                    'category_id' => 'required|numeric|exists:categories,id',
                    'code' => 'required|min:3|max:50|unique:products,code,' . $product->id,
                    'ean' => 'required|numeric',
                    'traing_price' => 'numeric',
                    'units_per_pack' => 'required|numeric',
                    'manufacturer' => 'string',
                    'discontinued' => 'required|numeric|in:0,1'
                ]);

            if($validator->fails())
            {
                $errors[strval($product->reference)] = $validator->errors();
                $invalidCount++;
                continue;
            }
                
            $product->save();

            if($images = json_decode($p['image_url'], true))
            {
                while(is_array($images) && isset($images[0]) && is_array($images[0]))
                    $images = $images[0];
            }
            else
                $images = [$p['image_url']];


            if($smallImages = json_decode($p['small_image_url'], true))
            {
                while(is_array($smallImages) && isset($smallImages[0]) && is_array($smallImages[0]))
                    $smallImages = $smallImages[0];
            }
            else
                $smallImages = [$p['small_image_url']];
            
            for($i = 0; $i < count($images) && $i < count($smallImages); $i++)
                //Check if that image is already on the DB.
                if(!Image::where([['product_id', $product->id], ['url', $images[$i]], ['small_url', $smallImages[$i]]])->count())
                    //Add image to the insert array. It will be inserted later.
                    $imagesToInsert[] = [
                        'product_id' => $product->id,
                        'url' => $images[$i],
                        'small_url' => $smallImages[$i],
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ];
                else
                    $imagesToInsertRepeated[] = [
                        'product_id' => $product->id,
                        'url' => $images[$i],
                        'small_url' => $smallImages[$i],
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ];

            //Add this product id to the images delete query (it will be deleted later).
            $deleteQuery->orWhere('product_id', $product->id);

            //Add taxes and discounts
            if($removeOldTaxes)
                $product->taxes()->detach();

            if(isset($request->taxes))
            {
                foreach($request->taxes as $t)
                {
                    try {
                        $product->taxes()->attach($t['id']);
                    } catch (Illuminate\Database\QueryException $e) {
                        
                    }
                }
            }

            if($removeOldDiscounts)
                $product->discounts()->detach();

            if(isset($request->discounts))
            {
                foreach($request->discounts as $d)
                {
                    try {
                        $product->discounts()->attach($d['id']);
                    } catch (Illuminate\Database\QueryException $e) {
                        
                    }
                    
                }
            }

            $validCount++;

        }

        if($removeOldImages)
            //Delete all the images (optimized)
            $deleteQuery->delete();

        //Insert all the images (optimized)
        //If we remove old images before inserting new ones, we have to insert all the images (included repeated images) because
        //we have removed old images.
        if($removeOldImages)
            Image::insert(array_merge($imagesToInsert, $imagesToInsertRepeated));
        else
            Image::insert($imagesToInsert);

        if($cleanCategories)
        {
            //Clean unused categories
            $unusedCategoriesIds = Category::has('products', '<', '1')->pluck('id')->all();
            Category::destroy($unusedCategoriesIds);
        }

        return response()->json(['valid_count' => $validCount, 'invalid_count' => $invalidCount, 'errors' => $errors]);
    }

    private function getFieldsValues($data, &$fields, &$values, $parent = null)
    {
        
        foreach($data as $key => $value)
        {
            array_push($fields, ":" . $key);
            array_push($values, $value);          
        }
    }

    private function replace($fields, $values, $subject)
    {
        if(count($fields) != count($values))
            return $subject;

        $s = $subject;

        for($i = 0; $i < count($fields); $i++)
        {
            $arr = array_values((array)($values[$i]));
            if(isset($arr[0]) && is_array($arr[0]))
            {
                $value = json_encode($arr);
            }
            else if (isset($arr[0]))
                $value = $arr[0];
            else
                $value = $values[$i];

            $s = str_ireplace($fields[$i], $value, $s);
        }

        return $s;
    }

    //Method : POST
    //Relates two products.
    public function relate(\App\Http\Requests\ProductRelateRequest $request)
    {
        Product::relate($request->new_product, $request->previous_products);

        return response(null, 200);
    }


    //Method : GET
    //Get N related products
    public function getRelated($id, $n)
    {
        return Product::getRelated($id, $n);
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Ticket;
use App\TicketAnswer;

use JWTAuth;

class TicketAnswerController extends Controller
{
    public function __construct()
    {
        $this->middleware('ability:Admin|User,ticketanswer.index', ['only' => ['index']]);
        $this->middleware('ability:Admin|User,ticketanswer.store', ['only' => ['store']]);
        $this->middleware('ability:Admin|User,ticketanswer.show', ['only' => ['show']]);
        $this->middleware('ability:Admin|User,ticketanswer.update', ['only' => ['update']]);
        $this->middleware('ability:Admin,ticketanswer.destroy', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($ticketId)
    {
        $v = Validator::make(['id' => $ticketId], [
                'id' => 'required|integer|exists:tickets,id'
            ]);

        if($v->fails())
            return response($v->errors(), 402);

        $user = JWTAuth::parseToken()->authenticate();
        $ticket = Ticket::find($ticketId);

        //Users can only list ticket answers that belong to a ticket where that user is ticket user/agent.
        if($ticket->user->id != $user->id && $ticket->agent->id != $user->id)
            return response('Forbidden', 403);

        return response($ticket->ticketAnswers, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\TicketAnswerStoreRequest $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $ticket = Ticket::find($request->ticket_id);

        if($ticket->user->id != $user->id && (!$ticket->agent || $ticket->agent->id != $user->id))
            return response('Forbidden', 403);

        if($ticket->status == -1 || $ticket->status == 3)
            return response(['You cannot answer anymore'], 403);

        $ticketAnswer = new TicketAnswer();
        $ticketAnswer->message = $request->message;
        $ticketAnswer->user()->associate($user);
        $ticketAnswer->ticket()->associate($ticket);
        $ticketAnswer->save();

        //Set the status to Waiting when the user replies.
        if($ticket->status['id'] == 2 && $user->id == $ticket->user->id)
        {
            $ticket->status = 1;
            $ticket->save();
        }
            
        //Set the status to Answered when the agent answer.
        if($ticket->status['id'] == 1 && $ticket->agent && $user->id == $ticket->agent->id)
        {
            $ticket->status = 2;
            $ticket->save();
        }
            

        return response($ticketAnswer, 201);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ticketAnswer = TicketAnswer::with(['ticket', 'user'])->find($id); 

        if(!$ticketAnswer)
            return response(null, 200);

        $user = JWTAuth::parseToken()->authenticate();

        if($ticketAnswer->ticket->user->id != $user->id && $ticketAnswer->ticket->agent->id != $user->id)
            return response('Forbidden', 403);

        return response($ticketAnswer);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\TicketAnswerUpdateRequest $request, $id)
    {
        $v = Validator::make(['id' => $id], [
                'id' => 'required|integer|exists:ticket_answers,id'
            ]);

        if($v->fails())
            return response($v->errors(), 402);

        $user = JWTAuth::parseToken()->authenticate();
        $ticketAnswer = TicketAnswer::find($id);

        if($ticketAnswer->user->id != $user->id)
            return response('Forbidden', 403);

        $ticketAnswer->message = $request->message;
        $ticketAnswer->save();

        return response($ticketAnswer, 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $v = Validator::make(['id' => $id], [
                'id' => 'required|integer|exists:ticket_answers,id'
            ]);

        if($v->fails())
            return response($v->errors(), 402);

        $user = JWTAuth::parseToken()->authenticate();
        $ticketAnswer = TicketAnswer::find($id);

        if($ticketAnswer->user->id != $user->id)
            return response('Forbidden', 403);

        Ticket::destroy($id);

        return response(null, 200);
    }
}

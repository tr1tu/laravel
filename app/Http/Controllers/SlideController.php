<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Slide;

use Validator;

class SlideController extends Controller
{
    public function __construct()
    {
        $this->middleware('ability:Admin,slide.index', ['only' => ['index']]);
        $this->middleware('ability:Admin,slide.store', ['only' => ['store']]);
        $this->middleware('ability:Admin,slide.storemulti', ['only' => ['storeMulti']]);
        $this->middleware('ability:Admin,slide.show', ['only' => ['show']]);
        $this->middleware('ability:Admin,slide.update', ['only' => ['update']]);
        $this->middleware('ability:Admin,slide.destroy', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Slide::orderBy('order', 'asc')->get(), 200);
    }

    public function indexActive()
    {
        return response(Slide::getActive()->orderBy('order', 'asc')->get(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\SlideStoreRequest $request)
    {
        $slide = new Slide();
        $slide->title = $request->title;
        $slide->text = $request->text;
        $slide->image_url = $request->image_url;
        $slide->active = $request->active;
        $slide->order = $request->order;
        $slide->start_date = $request->start_date;
        $slide->end_date = $request->end_date;
        $slide->link = $request->link;

        $slide->save();

        return response($slide, 201);
    }

    public function storeMulti(\App\Http\Requests\SlideStoreMultiRequest $request)
    {
        $slidesToInsert = [];
        $errors = [];
        $invalidCount = 0;
        $order = 0;

        if($request->slides)
            foreach($request->slides as $slide)
            {
                $v = Validator::make($slide, [
                    'title' => 'required|string|min:3|max:100',
                    'text' => 'required|string|min:3|max:800',
                    'image_url' => 'required|string|max:300',
                    'active' => 'required|boolean',
                    'start_date' => 'required|date',
                    'end_date' => 'required|date'
                ]);

                if($v->fails())
                {
                    $errors[$order] = $v->errors();
                    $invalidCount++;
                    continue;
                }

                
                
                $slidesToInsert[] = [
                    'title' => $slide['title'],
                    'text' => $slide['text'],
                    'image_url' => $slide['image_url'],
                    'active' => $slide['active'],
                    'order' => $order++,
                    'start_date' => $slide['start_date'],
                    'end_date' => $slide['end_date'],
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'link' => isset($slide['link']) ? $slide['link'] : null
                ];

            }

        if($invalidCount <= 0)
        {
            //Delete all slides
            $oldSlides = Slide::all();
            foreach($oldSlides as $slide)
                $slide->delete();

            Slide::insert($slidesToInsert);

            return response(null, 201);
        }

        return response(['errors' => $errors], 400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $slide = Slide::find($id);
        return response($slide, $slide ? 200 : 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $v = Validator::make(['id' => $id], [
                'id' => 'required|integer|exists:slides,id'
            ]);

        if($v->fails())
            return response($v->errors(), 402);

        $slide = Slide::find($id);
        if($request->title) $slide->title = $request->title;
        if($request->text) $slide->text = $request->text;
        if($request->image_url) $slide->image_url = $request->image_url;
        if($request->active) $slide->active = $request->active;
        if($request->order) $slide->order = $request->order;
        if($request->start_date) $slide->start_date = $request->start_date;
        if($request->end_date) $slide->end_date = $request->end_date;
        if($request->link) $slide->link = $request->link;
        

        $slide->save();

        return response($slide, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Slide::destroy($id);
        return response(null, 200);
    }
}

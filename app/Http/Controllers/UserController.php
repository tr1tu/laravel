<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;

use Illuminate\Support\Facades\Hash;

use JWTAuth;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('ability:Admin,user.index', ['only' => ['index']]);
        $this->middleware('ability:Admin,user.store', ['only' => ['store']]);
        $this->middleware('ability:Admin,user.show', ['only' => ['show']]);
        $this->middleware('ability:Admin|User,user.showself', ['only' => ['showSelf']]);
        $this->middleware('ability:Admin,user.update', ['only' => ['update']]);
        $this->middleware('ability:Admin|User,user.updateself', ['only' => ['updateSelf']]);
        $this->middleware('ability:Admin,user.destroy', ['only' => ['destroy']]);
        $this->middleware('ability:Admin|User,user.destroyself', ['only' => ['destroySelf']]);
        $this->middleware('ability:Admin,user.clearRoles', ['only' => ['clearRoles']]);


    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(User::all(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\UserStoreRequest $request)
    {
        $user = new User();
        $user->username = $request->username;
        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->dnicif = $request->dnicif;
        $user->phone = $request->phone;
        $user->mobphone = $request->mobphone;
        $user->fax = $request->fax;
        $user->country = $request->country;
        $user->email = $request->email;
        User::setPassword($user, $request->password);
        $user->activated = 1;
        $user->expired_password = false;
        $user->save();
        return response($user,201);
    }

    public function register(\App\Http\Requests\UserRegisterRequest $request)
    {
        $user = new User();
        $user->username = $request->username;
        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->dnicif = $request->dnicif;
        $user->phone = $request->phone;
        $user->mobphone = $request->mobphone;
        $user->fax = $request->fax;
        $user->country = $request->country;
        $user->email = $request->email;
        User::setPassword($user, $request->password);
        $user->activated = 1;
        $user->expired_password = false;
        $user->save();
        return response($user,201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(User::with(['addresses','roles'])->find($id), 200);
    }

    //Muestra el propio usuario
    public function showSelf()
    {
        $user = JWTAuth::parseToken()->authenticate();
        return response(User::with(['addresses'])->find($user->id), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\UserUpdateRequest $request, $id)
    {
        $user = User::find($id);
        if($request->username) $user->username = $request->username;
        if($request->password) User::setPassword($user, $request->password);
        if($request->name) $user->name = $request->name;
        if($request->lastname) $user->lastname = $request->lastname;
        if($request->dnicif) $user->dnicif = $request->dnicif;
        if($request->phone) $user->phone = $request->phone;
        if($request->mobphone) $user->mobphone = $request->mobphone;
        if($request->fax) $user->fax = $request->fax;
        if($request->activated) $user->activated = $request->activated;
        if($request->country) $user->country = $request->country;
        if($request->email) $user->email = $request->email;
        if(isset($request->expired_password)) $user->expired_password = $request->expired_password;
        $user->save();
        return response($user, 200);
    }

    //Actualiza el propio usuario
    public function updateSelf(\App\Http\Requests\UserUpdateSelfRequest $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        $credentials = ['email' => $user->email, 'password' => $request->current_password . $user->salt ];
        
        try {
            if (! $token = JWTAuth::attempt($credentials)) 
            {
                return response()->json(['error' => 'Current password is not valid'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
   
        if($request->username) $user->username = $request->username;
        if($request->password)
        {
            User::setPassword($user, $request->password);
            $user->expired_password = false;
        }
        if($request->name) $user->name = $request->name;
        if($request->group) $user->group = $request->group;
        if($request->lastname) $user->lastname = $request->lastname;
        if($request->dnicif) $user->dnicif = $request->dnicif;
        if($request->phone) $user->phone = $request->phone;
        if($request->mobphone) $user->mobphone = $request->mobphone;
        if($request->fax) $user->fax = $request->fax;
        if($request->activated) $user->activated = $request->activated;
        if($request->country) $user->country = $request->country;
        if($request->email) $user->email = $request->email;
        $user->save();
        return response($user, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        return response(null, 200);
    }

    //Borra el propio usuario
    public function destroySelf()
    {
        $user = JWTAuth::parseToken()->authenticate();
        User::destroy($user->id);
        return response(null, 200);
    }

    //Clear user roles for the user
    public function clearRoles($id)
    {
        $user = User::find($id);
        if(!$user)
            return response(['User not found'], 404);
            
        $user->roles()->detach();

        return response(null, 200);
    }
}

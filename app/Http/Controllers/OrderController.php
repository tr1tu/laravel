<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Order;
use App\Item;
use App\ItemTax;
use App\Cart;
use App\Address;
use App\Shipment;
use App\Invoice;
use App\Config;

use JWTAuth;

use PayPal\Api\Payer;
use PayPal\Api\Item as PayPalItem;
use PayPal\Api\ItemList;
use PayPal\Api\Details;
use PayPal\Api\Amount;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Payment as PayPalPayment;

use Mail;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('ability:Admin,order.index', ['only' => ['index']]);
        $this->middleware('ability:Admin,order.indexself', ['only' => ['indexSelf']]);
        $this->middleware('ability:Admin|User,order.store', ['only' => ['store']]);
        $this->middleware('ability:Admin,order.show', ['only' => ['show']]);
        $this->middleware('ability:Admin|User,order.showself', ['only' => ['showSelf']]);
        $this->middleware('ability:Admin,order.update', ['only' => ['update']]);
        $this->middleware('ability:Admin,order.updatestatus', ['only' => ['updateStatus']]);
        $this->middleware('ability:Admin,order.destroy', ['only' => ['destroy']]);
        $this->middleware('ability:Admin|User,order.additemsfromcart', ['only' => ['addItemsFromCart']]);
        $this->middleware('ability:Admin,order.sendcustomeremail', ['only' => ['sendCustomerEmail']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Order::all(), 200);
    }

    public function indexSelf()
    {
        $user = JWTAuth::parseToken()->authenticate();
        $orders = $user->orders()->with(['user'])->orderBy('id', 'DESC')->get();

        return response($orders, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\OrderStoreUpdateRequest $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        $order = new Order();
        $order->user()->associate($user);

        $bAddress = Address::find($request->bAddressId);
        $sAddress = Address::find($request->sAddressId);

        if($bAddress->user->id != $user->id || $sAddress->user->id != $user->id)
            return response(['unauthorized'], 403);

        $shipment = Shipment::find($request->shipmentId);

        $order->ba_name = $bAddress->name;
        $order->ba_lastname = $bAddress->lastname;
        $order->ba_dnicif = $bAddress->dnicif;
        $order->ba_country = $bAddress->country;
        $order->ba_province = $bAddress->province;
        $order->ba_city = $bAddress->city;
        $order->ba_zip_code = $bAddress->zip_code;
        $order->ba_phone = $bAddress->phone;
        $order->ba_mob_phone = $bAddress->mob_phone;
        $order->ba_address = $bAddress->address;
        $order->sa_name = $sAddress->name;
        $order->sa_lastname = $sAddress->lastname;
        $order->sa_dnicif = $sAddress->dnicif;
        $order->sa_country = $sAddress->country;
        $order->sa_province = $sAddress->province;
        $order->sa_city = $sAddress->city;
        $order->sa_zip_code = $sAddress->zip_code;
        $order->sa_phone = $sAddress->phone;
        $order->sa_mob_phone = $sAddress->mob_phone;
        $order->sa_address = $sAddress->address;
        $order->sm_name = $shipment->name;
        $order->sm_fee = $shipment->fee;
        $order->status = 1;

        $order->save();

        $cart = $user->cart;

        foreach($cart->products as $product)
        {
            Order::addItem($order->id, $product->id, $product->pivot->ammount);
        }

        $order = Order::find($order->id);
        
        //Clear cart
        $cart->products()->detach();


        return response($this->checkout($order, [
            'paymentId' => $request->paymentId,
            'success_redirect' => $request->success_redirect,
            'cancel_redirect' => $request->cancel_redirect,
            'transfer_redirect' => $request->transfer_redirect
        ]), 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(Order::with(['user', 'items', 'invoice'])->find($id)->append(['tax_summary']), 200);
    }

    public function showSelf($id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $order = $user->orders()->with(['user', 'items', 'invoice'])->find($id)->append(['tax_summary']);

        return response($order, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\OrderStoreUpdateRequest $request, $id)
    {
        $order = Order::find($id);

        $bAddress = Address::find($request->bAddressId);
        $sAddress = Address::find($request->sAddressId);
        $shipment = Shipment::find($request->shipmentId);

        $order->ba_name = $bAddress->name;
        $order->ba_lastname = $bAddress->lastname;
        $order->ba_dnicif = $bAddress->dnicif;
        $order->ba_country = $bAddress->country;
        $order->ba_province = $bAddress->province;
        $order->ba_city = $bAddress->city;
        $order->ba_zip_code = $bAddress->zip_code;
        $order->ba_phone = $bAddress->phone;
        $order->ba_mob_phone = $bAddress->mob_phone;
        $order->ba_address = $bAddress->address;
        $order->sa_name = $sAddress->name;
        $order->sa_lastname = $sAddress->lastname;
        $order->sa_dnicif = $sAddress->dnicif;
        $order->sa_country = $sAddress->country;
        $order->sa_province = $sAddress->province;
        $order->sa_city = $sAddress->city;
        $order->sa_zip_code = $sAddress->zip_code;
        $order->sa_phone = $sAddress->phone;
        $order->sa_mob_phone = $sAddress->mob_phone;
        $order->sa_address = $sAddress->address;
        $order->sm_name = $shipment->name;
        $order->sm_fee = $shipment->fee;
        if($request->status) $order->status = $request->status;

        $order->save();

        return response(null, 200);
    }

    //PUT
    public function updateStatus(\App\Http\Requests\OrderUpdateStatusRequest $request, $id)
    {
        $order = Order::findOrFail($id);
        $order->status = $request->status;
        $order->save();

        return response($order->status, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Order::destroy($id);
        return response(null, 200);
    }

    //Create order items from cart items.
    public function addItemsFromCart(\App\Http\Requests\OrderAddItemsFromCartRequest $request)
    {
        $order = Order::find($request->order_id);
        $user = JWTAuth::parseToken()->authenticate();

        if($order->user->id != $user->id)
            return response(['unauthorized'], 403);

        $cart = $user->cart;

        foreach($cart->products as $product)
        {
            Order::addItem($request->order_id, $product->id, $product->pivot->ammount);
        }
    }

    public function finishPayment(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $order = Order::find($request->orderId);

        if(!$order)
            return response(['order_not_found'], 404);

        if($order->user->id != $user->id)
            return response(['unauthorized'], 403);

        $cart = $user->cart;

        return $this->checkout($order, [
            'paymentId' => $order->p_id,
            'success_redirect' => $request->success_redirect,
            'cancel_redirect' => $request->cancel_redirect,
            'transfer_redirect' => $request->transfer_redirect
        ]);
    }

    public function checkout($order, $options = array())
    {
        $order->p_id = (int)$options['paymentId'];

        switch((int)$options['paymentId'])
        {
            case 1:
                //PayPal
                $order->p_name = 'PayPal';
                $order->save();
                return $this->checkoutPayPal($order,
                    rawurlencode($options['success_redirect']),
                    rawurlencode($options['cancel_redirect']));
            break;

            case 2:
                //Credit card
                return null;
            break;

            case 3:
                //Bank transfer
                $order->p_name = 'Transfer';
                $order->save();

                return $this->checkoutTransfer($order,
                    rawurlencode($options['transfer_redirect']));
            break;

            case 4:
                //Cash on delivery
                $order->p_name = 'COD';
                $order->save();
                return $this->checkoutCOD($order,
                    rawurlencode($options['success_redirect']),
                    rawurlencode($options['cancel_redirect']));
            break;
        }
    }

    public function checkoutPayPal($order, $successRedirect, $cancelRedirect)
    {
        $appKey = Config::get('paypal_app_key');
        $appSecret = Config::get('paypal_app_secret');
        $currency = Config::get('paypal_currency');
        $handlingFee = Config::get('paypal_handling_fee');
        $companyName = Config::get('company_name');


        $paypal = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                $appKey,
                $appSecret
            )
        );

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $itemList = new ItemList();

        foreach($order->items as $it)
        {
            $item = new PayPalItem();
            $item->setName($it->product->name)
            ->setCurrency($currency)
            ->setQuantity($it->ammount)
            ->setPrice($it->price_total);
            $itemList->addItem($item);
        }

    
        $details = new Details();
        $details->setShipping($order->sm_fee)
            ->setHandlingFee($handlingFee * $order->price_total)
            ->setSubtotal($order->price_total);
        $order->p_fee = $handlingFee * $order->price_total;

        $amount = new Amount();
        $amount->setCurrency($currency)
            ->setTotal($order->price_total + $order->sm_fee + $handlingFee * $order->price_total)
            ->setDetails($details);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription($companyName . ' ' . $order->id);


        $invoiceNumber = uniqid("",true);
        $transaction->setInvoiceNumber($invoiceNumber);
        $order->paypal_invoice_number = $invoiceNumber;

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl(url('/api/paypalpay') . '?url=' . $successRedirect)
            ->setCancelUrl(rawurldecode($cancelRedirect));


        $payment = new PayPalPayment();
        $payment->setIntent('sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions([$transaction]);

        try
        {
            $payment->create($paypal);
            $order->paypal_token = $payment->token;
            $order->status = 0;
            $order->save();
        }
        catch(Exception $e)
        {
            die($e);
        }

        $approvalUrl = $payment->getApprovalLink();

        return $approvalUrl;
    }

    //GET
    public function paypalPay(Request $request)
    {

        $appKey = \App\Config::get('paypal_app_key');
        $appSecret = \App\Config::get('paypal_app_secret');

        $paypal = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                $appKey,
                $appSecret
            )
        );

        

        $paymentId = $request->paymentId;
        $payerId = $request->PayerID;
        $url = rawurldecode($request->url);

        if(!strstr($url, 'http://') && !strstr($url, 'https://'))
            $url = 'http://' . $url;

        $payment = PayPalPayment::get($paymentId, $paypal);

        $order = Order::where('paypal_invoice_number', $payment->transactions[0]->invoice_number)->first();

        if(!$order || ($payment->transactions[0]->amount->getTotal() - ($order->price_total + $order->sm_fee + $order->p_fee) > 0.02) )
            return response(['Payment not valid'], 400);

        $order->paypal_payment_id = $paymentId;
        $order->paypal_payer_id = $payerId;
        $order->save();

        $execute = new PaymentExecution();
        $execute->setPayerId($payerId);

        try
        {
            $result = $payment->execute($execute, $paypal);
            $order->status = 2;
            $order->save();
            $invoice = new Invoice();
            $invoice->order()->associate($order);
            $invoice->save();

            return redirect($url);
        }
        catch(Exception $e)
        {
            die($e);
        }


        
    }

    public function checkoutCOD($order, $successUrl, $cancelUrl)
    {
        $order->status = 1;
        $order->save();

        return $successUrl;
    }

    public function checkoutTransfer($order, $transferUrl)
    {
        $order->status = 0;
        $order->save();
        
        return $transferUrl;
    }

    //POST
    public function sendCustomerEmail(\App\Http\Requests\OrderCustomerEmailRequest $request)
    {
        $user = \App\User::findOrFail($request->user_id);

        Mail::send('emails.ordercustomer', [ 'content' => $request->message ], function($m) use ($user) {
            $m->from('test@app.dev', 'My shop')
                ->to($user->email, $user->name)->subject('Contact');
        });

        return response(null, 200);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Config;

class ConfigController extends Controller
{
	public function __construct()
	{
		$this->middleware('ability:Admin,address.index', ['only' => ['index']]);
		$this->middleware('ability:Admin,address.setmulti', ['only' => ['setMulti']]);
	}

	//GET
	public function getCompanyData()
	{
		$companyName = Config::get('company_name') ? Config::get('company_name') : '';
		$companyDnicif = Config::get('company_dnicif') ? Config::get('company_dnicif') : '';
		$companyAddress = Config::get('company_address') ? Config::get('company_address') : '';
		$companyLogo = Config::get('company_logo') ? Config::get('company_logo') : '';

		return response([
				'company_name' => $companyName,
				'company_dnicif' => $companyDnicif,
				'company_address' => $companyAddress,
				'company_logo' => $companyLogo
			],200);
	}

	//GET
	public function getOrderStatus()
	{
		$numSteps = Config::get('order_status_steps');

		$status = array();

		for($i = 0; $i < $numSteps; $i++)
		{
			array_push($status, [ 'id' => $i, 'name' => Config::get('order_status_text_' . $i) ]);
		}

		return response($status, 200);
	}

	//GET
	public function getOrderStatusCOD()
	{
		$numSteps = Config::get('order_status_cod_steps');

		$status = array();

		for($i = 0; $i < $numSteps; $i++)
		{
			array_push($status, [ 'id' => $i, 'name' => Config::get('order_status_cod_text_' . $i) ]);
		}

		return response($status, 200);
	}

	//GET
	public function getSocialMedia()
	{
		$socialMedia = Config::getMulti([
			'facebook_url',
			'twitter_url',
			'gplus_url',
			'pinterest_url',
			'youtube_url'
		]);

		return response($socialMedia, 200);
	}

	//GET
	public function getPaymentFees()
	{
		$paymentFees = Config::getMulti([
			'paypal_handling_fee', 'card_handling_fee', 'transfer_handling_fee', 'cod_handling_fee'
		]);

		return response($paymentFees, 200);
	}

	//GET
	public function getPaymentFinishedSteps()
	{
		return response(Config::getMulti(['payment_finished_step', 'payment_finished_step_cod']), 200);
	}

	public function index()
	{
		$configs = Config::all();
		$arr = [];

		foreach($configs as $c)
		{
			$arr[$c->key] = is_numeric($c->value) ? (int)$c->value : $c->value;
		}

		return response()->json((object)$arr);
	}

	public function setMulti(\App\Http\Requests\ConfigSetMultiRequest $request)
	{
		foreach($request->config as $key => $value)
		{
			Config::set($key, $value);
		}

		return response($request->config, 200);
	}
}

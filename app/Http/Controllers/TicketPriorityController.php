<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\TicketPriority;

use Validator;

class TicketPriorityController extends Controller
{
   public function __construct()
    {
        //$this->middleware('ability:Admin,ticketpriority.index', ['only' => ['index']]);
        $this->middleware('ability:Admin,ticketpriority.store', ['only' => ['store']]);
        //$this->middleware('ability:Admin,ticketpriority.show', ['only' => ['show']]);
        $this->middleware('ability:Admin,ticketpriority.update', ['only' => ['update']]);
        $this->middleware('ability:Admin,ticketpriority.destroy', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(TicketPriority::all(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\TicketPriorityStoreRequest $request)
    {
        $ticketPriority = new TicketPriority();
        $ticketPriority->name = $request->name;
        $ticketPriority->level = $request->level;
        $ticketPriority->save();

        return response($ticketPriority, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ticketPriority = TicketPriority::find($id);
        return response($ticketPriority, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\TicketPriorityUpdateRequest $request, $id)
    {
        $v = Validator::make(['id' => $id], [
                'id' => 'required|integer|exists:ticket_priorities,id'
            ]);

        if($v->fails())
            return response($v->errors(), 402);

        $ticketPriority = TicketPriority::find($id);
        $ticketPriority->name = $request->name;
        $ticketPriority->level = $request->level;
        $ticketPriority->save();

        return response($ticketPriority, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        TicketPriority::destroy($id);
        return response(null, 200);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\ImportProfile;
use App\Operation;
use App\ImportSetting;
use App\Tax;
use App\Discount;

use Validator;

class ImportProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware('ability:Admin,importprofile.index', ['only' => ['index']]);
        $this->middleware('ability:Admin,importprofile.store', ['only' => ['store']]);
        $this->middleware('ability:Admin,importprofile.show', ['only' => ['show']]);
        $this->middleware('ability:Admin,importprofile.update', ['only' => ['update']]);
        $this->middleware('ability:Admin,importprofile.destroy', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profiles = ImportProfile::with(['operations' => function($query) {
            $query->orderBy('import_profile_operation.order', 'ASC');
        }, 'taxes', 'discounts'])->get();

        return response($profiles, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\ImportProfileStoreRequest $request)
    {
        $profile = new ImportProfile();
        $profile->name = $request->name;
        $profile->supnodes = $request->supnodes;
        $profile->name_format = $request->name_format;
        $profile->description_format = $request->description_format;
        $profile->price_format = $request->price_format;
        $profile->stock_format = $request->stock_format;
        $profile->reference_format = $request->reference_format;
        $profile->category_format = $request->category_format;
        $profile->code_format = $request->code_format;
        $profile->ean_format = $request->ean_format;
        $profile->trading_price_format = $request->trading_price_format;
        $profile->units_per_pack_format = $request->units_per_pack_format;
        $profile->manufacturer_format = $request->manufacturer_format;
        $profile->discountinued_format = $request->discountinued_format;
        $profile->image_url_format = $request->image_url_format;
        $profile->small_image_url_format = $request->small_image_url_format;

        $profile->save();

        if(isset($request->operations))
        {
            $order = 0;
            $profile->operations()->detach();

            foreach($request->operations as $op)
            {
                $v = Validator::make($op, [
                        'name' => 'required|string|min:3|max:100',
                        'field' => 'required|string|max:80',
                        'type' => 'required|string|max:50',
                        'options' => 'required|array',
                        'iterations' => 'required|integer'
                ]);

                if($v->fails())
                    return response($v->errors(), 402);

                $operation = Operation::where('name', $op['name'])->first();

                if(!$operation)
                    $operation = new Operation();


                $operation->name = $op['name'];
                $operation->field = $op['field'];
                $operation->type = $op['type'];
                $operation->options = $op['options'];
                $operation->iterations = $op['iterations'];
                $operation->save();

                $profile->operations()->attach($operation, ['order' => $order]);
                $order++;

            }
        }

        

        if(isset($request->import_settings))
        {
            foreach($request->import_settings as $k => $v)
            {
                $profile->setSetting($k, $v);
            }
        }

        if(isset($request->taxes))
        {
            foreach($request->taxes as $tId)
            {
                $tax = Tax::find($tId);

                if($tax)
                    $profile->taxes()->attach($tax);
            }
        }

        if(isset($request->discounts))
        {
            foreach($request->discounts as $tId)
            {
                $discount = Discount::find($tId);

                if($discount)
                    $profile->discounts()->attach($discount);
            }
        }

        return response($profile, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = ImportProfile::with(['operations', 'taxes', 'discounts'])->getSettingsArray()->find($id);
        return response($profile, $profile ? 200 : 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $v = Validator::make(['id' => $id], [
                'id' => 'required|integer|exists:import_profiles,id'
            ]);

        if($v->fails())
            return response($v->errors(), 402);

        $profile = ImportProfile::find($id);
        
        if($request->name) $profile->name = $request->name;
        if($request->supnodes) $profile->supnodes = $request->supnodes;
        if($request->name_format) $profile->name_format = $request->name_format;
        if($request->description_format) $profile->description_format = $request->description_format;
        if($request->price_format) $profile->price_format = $request->price_format;
        if($request->stock_format) $profile->stock_format = $request->stock_format;
        if($request->reference_format) $profile->reference_format = $request->reference_format;
        if($request->category_format) $profile->category_format = $request->category_format;
        if($request->code_format) $profile->code_format = $request->code_format;
        if($request->ean_format) $profile->ean_format = $request->ean_format;
        if($request->trading_price_format) $profile->trading_price_format = $request->trading_price_format;
        if($request->units_per_pack_format) $profile->units_per_pack_format = $request->units_per_pack_format;
        if($request->manufacturer_format) $profile->manufacturer_format = $request->manufacturer_format;
        if($request->discountinued_format) $profile->discountinued_format = $request->discountinued_format;
        if($request->image_url_format) $profile->image_url_format = $request->image_url_format;
        if($request->small_image_url_format) $profile->small_image_url_format = $request->small_image_url_format;
        
        if($request->operations)
        {
            $profile->operations()->detach();
            $order = 0;
            foreach($request->operations as $op)
            {
                $v = Validator::make($op, [
                        'name' => 'required|string|min:3|max:100',
                        'field' => 'required|string|max:80',
                        'type' => 'required|string|max:50',
                        'options' => 'required|array',
                        'iterations' => 'required|integer'
                ]);

                if($v->fails())
                    return response($v->errors(), 402);

                $operation = Operation::where('name', $op['name'])->first();

                if(!$operation)
                    $operation = new Operation();


                $operation->name = $op['name'];
                $operation->field = $op['field'];
                $operation->type = $op['type'];
                $operation->options = $op['options'];
                $operation->iterations = $op['iterations'];
                $operation->save();

                $profile->operations()->attach($operation, ['order' => $order]);
                $order++;

            }
        }

        $profile->save();

        if(isset($request->import_settings))
        {
            //Clear old import settings
            $profile->importSettings()->delete();

            foreach($request->import_settings as $k => $v)
            {
                $profile->setSetting($k, $v);
            }
        }

        if(isset($request->taxes))
        {
            $profile->taxes()->detach();

            foreach($request->taxes as $t)
            {
                $tax = Tax::find($t['id']);

                if($tax)
                    $profile->taxes()->attach($tax);
            }
        }

        if(isset($request->discounts))
        {
            $profile->discounts()->detach();
            
            foreach($request->discounts as $d)
            {
                $discount = Discount::find($d['id']);

                if($discount)
                    $profile->discounts()->attach($discount);
            }
        }

        return response($profile, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ImportProfile::destroy($id);
        return response(null, 200);
    }
}

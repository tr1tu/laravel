<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Color;
use App\Product;

class ColorController extends Controller
{

    public function __construct()
    {
        $this->middleware('ability:Admin,color.index', ['only' => ['index']]);
        $this->middleware('ability:Admin,color.store', ['only' => ['store']]);
        $this->middleware('ability:Admin,color.show', ['only' => ['show']]);
        $this->middleware('ability:Admin,color.update', ['only' => ['update']]);
        $this->middleware('ability:Admin,color.destroy', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Color::all(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\ColorStoreRequest $request)
    {
        if($request->product_id)
        {
            $product = Product::find($request->product_id);

            if($product)
            {
                $color = new Color();

                if($request->name) $color->name = $request->name;
                if($request->image) $color->image = $request->image;
                if($request->RGB) $color->RGB = $request->RGB;

                $product->colors()->save($color);

                return response($color, 201);
            }

            return response("Product not found", 402);
        }

        return response("Product id required", 402);
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(Color::with(['product'])->find($id), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\ColorUpdateRequest $request, $id)
    {
        $color = Color::find($id);

        if($request->name) $color->name = $request->name;
        if($request->image) $color->image = $request->image;
        if($request->RGB) $color->RGB = $request->RGB;

        $color->save();
        return response(null, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Color::destroy($id);
        return response(null, 200);
    }
}

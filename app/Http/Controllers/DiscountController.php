<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Discount;

class DiscountController extends Controller
{

    public function __construct()
    {
        $this->middleware('ability:Admin,discount.index', ['only' => ['index']]);
        $this->middleware('ability:Admin,discount.store', ['only' => ['store']]);
        $this->middleware('ability:Admin,discount.show', ['only' => ['show']]);
        $this->middleware('ability:Admin,discount.update', ['only' => ['update']]);
        $this->middleware('ability:Admin,discount.destroy', ['only' => ['destroy']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Discount::all(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\DiscountStoreUpdateRequest $request)
    {
        $discount = new Discount();

        $discount->name = $request->name;
        $discount->type = $request->type;
        $discount->ammount = $request->ammount;
        $discount->permanent = $request->permanent;
        
        if($discount->permanent)
        {
            $discount->start_date = date('Y-m-d');
            $discount->end_date = date('Y-m-d');
        }
        else
        {
            $discount->start_date = $request->start_date;
            $discount->end_date = $request->end_date;
        }

        $discount->save();
        return response($discount, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(Discount::find($id), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\DiscountStoreUpdateRequest $request, $id)
    {
        $discount = Discount::find($id);

        $discount->name = $request->name;
        $discount->type = $request->type;
        $discount->ammount = $request->ammount;
        $discount->permanent = $request->permanent;
        
        if($discount->permanent)
        {
            $discount->start_date = date('Y-m-d');
            $discount->end_date = date('Y-m-d');
        }
        else
        {
            $discount->start_date = $request->start_date;
            $discount->end_date = $request->end_date;
        }

        $discount->save();
        return response(null, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Discount::destroy($id);
        return response(null, 200);
    }
}

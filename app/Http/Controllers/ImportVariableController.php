<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\ImportVariable;

class ImportVariableController extends Controller
{

    public function __construct()
    {
        $this->middleware('ability:Admin,importvariable.index', ['only' => ['index']]);
        $this->middleware('ability:Admin,importvariable.store', ['only' => ['store']]);
        $this->middleware('ability:Admin,importvariable.show', ['only' => ['show']]);
        $this->middleware('ability:Admin,importvariable.update', ['only' => ['update']]);
        $this->middleware('ability:Admin,importvariable.destroy', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $variables = ImportVariable::all();
        /*$arr = [];

        foreach($variables as $v)
        {
            $arr[$v->name] = $v->value;
        }

        return response($arr, 200);*/

        return response($variables, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\ImportVariableStoreRequest $request)
    {
        $value = $request->value != null ? $request->value : "";
        $variable = ImportVariable::set($request->name, $value);

        return response($variable, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $variable = ImportVariable::find($id);
        return response($variable, $variable != null ? 200 : 404);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Not required
        //Just leave this here to avoid errors.
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ImportVariable::destroy($id);
        return response(null, 200);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Opinion;

use JWTAuth;
use Validator;

class OpinionController extends Controller
{

    public function __construct()
    {
        //$this->middleware('ability:Admin|User,opinion.index', ['only' => ['index']]);
        $this->middleware('ability:Admin,opinion.store', ['only' => ['store']]);
        $this->middleware('ability:Admin|User,opinion.storeself', ['only' => ['storeSelf']]);
        //$this->middleware('ability:Admin|User,opinion.show', ['only' => ['show']]);
        $this->middleware('ability:Admin,opinion.update', ['only' => ['update']]);
        $this->middleware('ability:Admin|User,opinion.updateself', ['only' => ['updateSelf']]);
        $this->middleware('ability:Admin,opinion.destroy', ['only' => ['destroy']]);
        $this->middleware('ability:Admin|User,opinion.destroyself', ['only' => ['destroySelf']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Opinion::all(), 200);
    }

    //Method : GET
    //Returns all the opinions of a product.
    public function indexProduct($id, $pageSize = 15, $orderField = 'id', $orderType = 'asc')
    {
        return response(Opinion::fromProduct($id)->withUsername()->orderBy($orderField, $orderType)->paginate($pageSize), 200);
    }

    //Method : GET
    //Returns all the opinions posted by an user.
    public function indexUser($id)
    {
        return response(Opinion::fromUser($id)->get(), 200);
    }

    //Method : GET
    //Returns all the opinions from an user in a product.
    public function indexUserProduct($userId, $productId)
    {
        return response(Opinion::fromUser($userId)->fromProduct($productId)->get(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\OpinionStoreRequest $request)
    {
        $opinion = new Opinion();

        //We don't need to check if fields are present. We checked that in the request.
        $opinion->user()->associate($request->user_id);
        $opinion->product()->associate($request->product_id);
        $opinion->comments = $request->comments;
        $opinion->positive = $request->positive;
        $opinion->negative = $request->negative;
        $opinion->recommended = $request->recommended;
        $opinion->score = $request->score;

        $opinion->save();

        return response($opinion, 200);
    }

    //Method : POST
    //Post comment from current user
    public function storeSelf(\App\Http\Requests\OpinionStoreSelfRequest $request)
    {
        $opinion = new Opinion();

        $user = JWTAuth::parseToken()->authenticate();

        $opinion->user()->associate($user->id);
        $opinion->product()->associate($request->product_id);
        $opinion->comments = $request->comments;
        $opinion->positive = $request->positive;
        $opinion->negative = $request->negative;
        $opinion->recommended = $request->recommended;
        $opinion->score = $request->score;

        $opinion->save();

        return response($opinion, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(Opinion::with('product')->withUsername()->withVotes()->find($id), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\OpinionUpdateRequest $request, $id)
    {
       
        $v = Validator::make(['id' => $id], [
                'id' => 'required|integer|exists:opinions,id'
            ]);

        if($v->fails())
            return response($v->errors(), 402);

        $opinion = Opinion::find($id);

       //We don't need to check if fields are present. We checked that in the request.
        $opinion->comments = $request->comments;
        $opinion->positive = $request->positive;
        $opinion->negative = $request->negative;
        $opinion->recommended = $request->recommended;
        $opinion->score = $request->score;

        $opinion->save();

        return response($opinion, 200); 

        
    }

    //Method : PUT
    //Updates opinion made by the same user.
    public function updateSelf(\App\Http\Requests\OpinionUpdateRequest $request, $id)
    {
      
        $v = Validator::make(['id' => $id], [
                'id' => 'required|integer|exists:opinions,id'
            ]);

        if($v->fails())
            return response($v->errors(), 402);

        $user = JWTAuth::parseToken()->authenticate();

        $opinion = Opinion::find($id);

        if($opinion->user->id != $user->id)
            return response('Forbidden', 403);

        $opinion->comments = $request->comments;
        $opinion->positive = $request->positive;
        $opinion->negative = $request->negative;
        $opinion->recommended = $request->recommended;
        $opinion->score = $request->score;

        $opinion->save();

        return response($opinion, 200); 

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Opinion::destroy($id);
        return response(null, 200);
    }

    //Method : DELETE
    //Deletes opinion made by the same user.
    public function destroySelf($id)
    {
        $user = JWTAuth::parseToken()->authenticate();

        $opinion = \App\Opinion::find($id);

        if($opinion->user->id != $user->id)
            return response('Forbidden', 403);

        Opinion::destroy($id);
        return response(null, 200);
    }

    //Method : POST
    //Upvote opinion
    public function upvote(\App\Http\Requests\OpinionVoteRequest $request)
    {
        
        $opinion = Opinion::upvote($request->opinion_id, $request->user_id);

        return response($opinion, 200);

    }

    //Method : POST
    //Downvote opinion
    public function downvote(\App\Http\Requests\OpinionVoteRequest $request)
    {
        
        $opinion = Opinion::downvote($request->opinion_id, $request->user_id);

        return response($opinion, 200);

    }
}

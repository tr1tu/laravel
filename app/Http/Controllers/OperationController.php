<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Operation;

use Validator;

class OperationController extends Controller
{

    public function __construct()
    {
        $this->middleware('ability:Admin,operation.index', ['only' => ['index']]);
        $this->middleware('ability:Admin,operation.store', ['only' => ['store']]);
        $this->middleware('ability:Admin,operation.show', ['only' => ['show']]);
        $this->middleware('ability:Admin,operation.update', ['only' => ['update']]);
        $this->middleware('ability:Admin,operation.destroy', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Operation::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\OperationStoreRequest $request)
    {
        $found = Operation::where('name', $request->name)
                            ->where('field', $request->field)
                            ->where('type', $request->type)
                            ->where('options', $request->options)
                            ->where('iterations', $request->iterations)
                            ->first();

        if($found)
            return response($found, 200);

        $op = new Operation();

        $op->name = $request->name;
        $op->field = $request->field;
        $op->type = $request->type;
        $op->options = $request->options;
        $op->iterations = $request->iterations;

        $op->save();

        return response($op, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $op = Operation::find($id);
        return response($op, $op ? 200 : 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\OperationUpdateRequest $request, $id)
    {
         $v = Validator::make(['id' => $id], [
                'id' => 'required|integer|exists:operations,id'
            ]);

        if($v->fails())
            return response($v->errors(), 402);

        $op = Operation::find($id);

        if($request->name) $op->name = $request->name;
        if($request->field) $op->field = $request->field;
        if($request->type) $op->type = $request->type;
        if($request->options) $op->options = $request->options;
        if($request->iterations) $op->iterations = $request->iterations;

        $op->save();

        return response($op, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Operation::destroy($id);
        return response(null, 200);
    }
}

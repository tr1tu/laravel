<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Ticket;
use App\TicketCategory;
use App\TicketPriority;
use Validator;

use JWTAuth;

class TicketController extends Controller
{

    public function __construct()
    {
        $this->middleware('ability:Admin,ticket.index', ['only' => ['index']]);
        $this->middleware('ability:Admin|User,ticket.indexself', ['only' => ['indexSelf']]);
        $this->middleware('ability:Admin|User,ticket.store', ['only' => ['store']]);
        $this->middleware('ability:Admin,ticket.show', ['only' => ['show']]);
        $this->middleware('ability:Admin|User,ticket.showself', ['only' => ['showSelf']]);
        $this->middleware('ability:Admin,ticket.update', ['only' => ['update']]);
        $this->middleware('ability:Admin|User,ticket.updateself', ['only' => ['updateSelf']]);
        $this->middleware('ability:Admin,ticket.destroy', ['only' => ['destroy']]);
        $this->middleware('ability:Admin|User,ticket.cancelSelf', ['only' => ['cancelSelf']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Ticket::with(['ticketCategory', 'ticketPriority'])->get(), 200);
    }

    /**
     * Display a listing of tickets of the current user.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexSelf()
    {
        $user = JWTAuth::parseToken()->authenticate();
        return response(Ticket::with(['ticketCategory', 'ticketPriority'])->where('user_id', $user->id)->get(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\TicketStoreRequest $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $ticketCategory = TicketCategory::find($request->ticket_category_id);
        $ticketPriority = TicketPriority::find($request->ticket_priority_id);

        $ticket = new Ticket();
        $ticket->user()->associate($user);
        $ticket->TicketCategory()->associate($ticketCategory);
        $ticket->TicketPriority()->associate($ticketPriority);
        $ticket->message = $request->message;
        $ticket->subject = $request->subject;
        Ticket::setStatusWaiting($ticket);
        
        $ticket->save();

        return response($ticket, 201);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ticket = Ticket::with(['user', 'agent','ticketCategory', 'ticketPriority'])->with(['ticketAnswers' => function($query) {
            return $query->with(['user'])->orderBy('created_at', 'DESC');
        }])->find($id);
        return response($ticket, $ticket ? 200 : 404);
    }

    /**
     * Display the specified ticket of the current user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showSelf($id)
    {
        $ticket = Ticket::with(['user', 'agent','ticketCategory', 'ticketPriority', 'ticketAnswers'])
                        ->with(['ticketAnswers.user' => function($query) {
                            $query->select(['id', 'username', 'name', 'lastname', 'email']);
                        }])
                        ->find($id);
        if($ticket != null)
        {
            $user = JWTAuth::parseToken()->authenticate();

            if($user->id != $ticket->user->id)
                return response('Forbidden', 403);

            return response($ticket, 200);
        } 

        return response(null, 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\TicketUpdateRequest $request, $id)
    {
        $v = Validator::make(['id' => $id], [
                'id' => 'required|integer|exists:tickets,id'
            ]);

        if($v->fails())
            return response($v->errors(), 402);

        $ticket = Ticket::find($id);

        if($request->message) $ticket->message = $request->message;
        if($request->subject) $ticket->subject = $request->subject;
        if(isset($request->status)) $ticket->status = $request->status;
        if($request->ticket_category_id)
        {
            $ticketCategory = TicketCategory::find($request->ticket_category_id);
            $ticket->TicketCategory()->associate($ticketCategory);
        }
        if($request->ticket_priority_id)
        {
            $ticketPriority = TicketPriority::find($request->ticket_priority_id);
            $ticket->TicketPriority()->associate($ticketPriority);
        }
        if($request->agent_id)
        {
            $ticket->agent()->associate($request->agent_id);
        }

        $ticket->save();

        return response($ticket, 200);

    }

    /**
     * Update the specified ticket of the current user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateSelf(\App\Http\Requests\TicketUpdateRequest $request, $id)
    {
        $v = Validator::make(['id' => $id], [
                'id' => 'required|integer|exists:tickets,id'
            ]);

        if($v->fails())
            return response($v->errors(), 402);

        $ticket = Ticket::find($id);
        $user = JWTAuth::parseToken()->authenticate();

        if($ticket->user->id != $user->id)
            return response('Forbidden', 403);

        if($ticket->status->id != 1)
            return response(['You cannot modify this ticket anymore'], 402);

        if($request->message) $ticket->message = $request->message;
        if($request->ticket_category_id)
        {
            $ticketCategory = TicketCategory::find($request->ticket_category_id);
            $ticket->TicketCategory()->associate($ticketCategory);
        }
        if($request->ticket_priority_id)
        {
            $ticketPriority = TicketPriority::find($request->ticket_priority_id);
            $ticket->TicketPriority()->associate($ticketPriority);
        }

        $ticket->save();

        return response($ticket, 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Ticket::destroy($id);
        return response(null, 200);
    }

    /*
        Cancels a specified ticket of the current user.
    */
    public function cancelSelf($id)
    {
        $v = Validator::make(['id' => $id], [
                'id' => 'required|integer|exists:tickets,id'
            ]);
        
         if($v->fails())
            return response($v->errors(), 400);

        $ticket = Ticket::find($id);
        $user = JWTAuth::parseToken()->authenticate();

        if($ticket->user->id != $user->id)
            return response(['Forbidden'], 403);

        if($ticket->status['id'] == 0 || $ticket->status['id'] == 3)
            return response(['Ticket is not opened'], 400);
        
        Ticket::setStatusCancelled($ticket);
        $ticket->save();

        return response($ticket, 200);
    }
}

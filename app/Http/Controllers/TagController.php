<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Tag;

class TagController extends Controller
{

    public function __construct()
    {
        $this->middleware('ability:Admin,tag.index', ['only' => ['index']]);
        $this->middleware('ability:Admin,tag.store', ['only' => ['store']]);
        $this->middleware('ability:Admin,tag.show', ['only' => ['show']]);
        $this->middleware('ability:Admin,tag.update', ['only' => ['update']]);
        $this->middleware('ability:Admin,tag.destroy', ['only' => ['destroy']]);
    }   

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Tag::all(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\Request\TagStoreUpdateRequest $request)
    {
        $tag = new Tag();

        if($request->name) $tag->name = $request->name;

        $tag->save();

        return response($tag, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(Tag::with(['products', 'categories'])->find($id), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\Request\TagStoreUpdateRequest $request, $id)
    {
        $tag = Tag::find($id);

        if($request->name) $tag->name = $request->name;

        $tag->save();

        return response(null, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Tag::destroy($id);
        return response(null, 200);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Tax;

class TaxController extends Controller
{

    public function __construct()
    {
        $this->middleware('ability:Admin,tax.index', ['only' => ['index']]);
        $this->middleware('ability:Admin,tax.store', ['only' => ['store']]);
        $this->middleware('ability:Admin,tax.show', ['only' => ['show']]);
        $this->middleware('ability:Admin,tax.update', ['only' => ['update']]);
        $this->middleware('ability:Admin,tax.destroy', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Tax::all(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\TaxStoreUpdateRequest $request)
    {
        $tax = new Tax();

        if($request->tax) $tax->tax = $request->tax;
        if($request->name) $tax->name = $request->name;
        
        $tax->save();
        return response($tax, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(Tax::find($id), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\TaxStoreUpdateRequest $request, $id)
    {
        $tax = Tax::find($id);

        if($request->tax) $tax->tax = $request->tax;
        if($request->name) $tax->name = $request->name;

        $tax->save();
        return response(null, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Tax::destroy($id);
        return response(null, 200);
    }
}

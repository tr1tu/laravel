<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Image;
use App\Product;

class ImageController extends Controller
{

    public function __construct() 
    {
        $this->middleware('ability:Admin,image.index', ['only' => ['index']]);
        $this->middleware('ability:Admin,image.store', ['only' => ['store']]);
        $this->middleware('ability:Admin,image.show', ['only' => ['show']]);
        $this->middleware('ability:Admin,image.update', ['only' => ['update']]);
        $this->middleware('ability:Admin,image.destroy', ['only' => ['destroy']]);
        $this->middleware('ability:Admin,image.clearorder', ['only' => ['clearOrder']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Image::all(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\ImageStoreRequest $request)
    {
        $image = new Image();
        $product = null;

        if($request->product_id) $product = Product::find($request->product_id);
        if($request->url) $image->url = $request->url;
        if($request->small_url) $image->small_url = $request->small_url;
        if($request->description) $image->description = $request->description;
        if($request->order) $image->order = $request->order;


        if($product != null)
        {

            $image->product()->associate($product);
            $image->save();

            return response($image, 200);
        }
        else
        {
            return response("Product not found", 402);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(Image::find($id)->with(['product']), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\ImageUpdateRequest $request, $id)
    {
        $image = Image::find($id);

        if($request->url) $image->url = $request->url;
        if($request->small_url) $image->small_url = $request->small_url;
        if($request->description) $image->description = $request->description;
        if($request->order) $image->order = $request->order;

        $image->save();

        return response(null, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Image::destroy($id);
        return response(null, 200);
    }


    //Method : DELETE
    //Set the order of all the images of a given product to null.
    public function clearOrder($product_id)
    {
        Image::where('product_id', '=', $product_id)->update(['order' => null]);
        return response(null, 200);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\TicketCategory;

use Validator;

class TicketCategoryController extends Controller
{
    public function __construct()
    {
        //$this->middleware('ability:Admin,ticketcategory.index', ['only' => ['index']]);
        $this->middleware('ability:Admin,ticketcategory.store', ['only' => ['store']]);
        //$this->middleware('ability:Admin,ticketcategory.show', ['only' => ['show']]);
        $this->middleware('ability:Admin,ticketcategory.update', ['only' => ['update']]);
        $this->middleware('ability:Admin,ticketcategory.destroy', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(TicketCategory::all(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\TicketCategoryStoreUpdateRequest $request)
    {
        $ticketCategory = new TicketCategory();
        $ticketCategory->name = $request->name;
        $ticketCategory->save();

        return response($ticketCategory, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ticketCategory = TicketCategory::find($id);
        return response($ticketCategory, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $v = Validator::make(['id' => $id], [
                'id' => 'required|integer|exists:ticket_categories,id'
            ]);

        if($v->fails())
            return response($v->errors(), 402);

        $ticketCategory = TicketCategory::find($id);
        $ticketCategory->name = $request->name;
        $ticketCategory->save();

        return response($ticketCategory, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        TicketCategory::destroy($id);
        return response(null, 200);
    }
}

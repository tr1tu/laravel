<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Sizing;

class SizingController extends Controller
{

    public function __construct()
    {
        $this->middleware('ability:Admin,sizing.index', ['only' => ['index']]);
        $this->middleware('ability:Admin,sizing.store', ['only' => ['store']]);
        $this->middleware('ability:Admin,sizing.show', ['only' => ['show']]);
        $this->middleware('ability:Admin,sizing.update', ['only' => ['update']]);
        $this->middleware('ability:Admin,sizing.destroy', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Sizing::all(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\SizingStoreUpdateRequest $request)
    {
        $sizing = new Sizing();

        if($request->sizing_number) $sizing->sizing_number = $request->sizing_number;
        if($request->name) $sizing->name = $request->name;
        
        $sizing->save();
        return response($sizing, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(Sizing::with('sizes')->find($id), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\SizingStoreUpdateRequest $request, $id)
    {
        $sizing = Sizing::find($id);

        if($request->sizing_number) $sizing->sizing_number = $request->sizing_number;
        if($request->name) $sizing->name = $request->name;

        $sizing->save();
        return response(null, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Sizing::destroy($id);
        return response(null, 200);
    }
}

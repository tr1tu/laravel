<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Size;
use App\Sizing;

class SizeController extends Controller
{

    public function __construct()
    {
        $this->middleware('ability:Admin,size.index', ['only' => ['index']]);
        $this->middleware('ability:Admin,size.store', ['only' => ['store']]);
        $this->middleware('ability:Admin,size.show', ['only' => ['show']]);
        $this->middleware('ability:Admin,size.update', ['only' => ['update']]);
        $this->middleware('ability:Admin,size.destroy', ['only' => ['destroy']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Size::all(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\SizeStoreUpdateRequest $request)
    {
        if($request->sizing_id)
        {
            $sizing = Sizing::find($request->sizing_id);

            if($sizing)
            {
                 $size = new Size();

                if($request->size) $size->size = $request->size;
                if($request->measure) $size->measure = $request->measure;

                $sizing->sizes()->save($size);

                return response($size, 201);
            }

            return response('Sizing not found', 402);
        }
       
        return response('Sizing id required', 402);

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(Size::with('sizing')->find($id), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\SizeStoreUpdateRequest $request, $id)
    {
        $size = Size::find($id);

        if($request->size) $size->size = $request->size;
        if($request->measure) $size->measure = $request->measure;

        $size->save();
        return response(null, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Size::destroy($id);
        return response(null, 200);
    }
}

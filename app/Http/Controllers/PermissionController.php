<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Permission;

class PermissionController extends Controller
{

    public function __construct()
    {
        $this->middleware('ability:Admin,permission.index', ['only' => ['index']]);
        $this->middleware('ability:Admin,permission.store', ['only' => ['store']]);
        $this->middleware('ability:Admin,permission.show', ['only' => ['show']]);
        $this->middleware('ability:Admin,permission.update', ['only' => ['update']]);
        $this->middleware('ability:Admin,permission.destroy', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Permission::all(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\PermissionStoreUpdateRequest $request)
    {
       $permission = new Permission();
       if($request->name != null) $permission->name = $request->name;
       if($request->display_name != null) $permission->display_name = $request->display_name; 
       if($request->description != null) $permission->description = $request->description; 
       $permission->save();

       return response($permission, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(Permission::find($id), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\PermissionStoreUpdateRequest $request, $id)
    {
        $permission = Permission::find($id);
        if($request->name != null) $permission->name = $request->name;
        if($request->display_name != null) $permission->display_name = $request->display_name; 
        if($request->description != null) $permission->description = $request->description; 
        $permission->save();

        return response(null, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Permission::destroy($id);
        return response(null, 200);
    }
}

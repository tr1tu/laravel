<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Payment;

class PaymentController extends Controller
{

    public function __construct() 
    {
        $this->middleware('ability:Admin|User,payment.index', ['only' => ['index']]);
        $this->middleware('ability:Admin,payment.store', ['only' => ['store']]);
        $this->middleware('ability:Admin|User,payment.show', ['only' => ['show']]);
        $this->middleware('ability:Admin,payment.update', ['only' => ['update']]);
        $this->middleware('ability:Admin,payment.destroy', ['only' => ['destroy']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Payment::all(), 200); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\PaymentStoreUpdateRequest $request)
    {
        $payment = new Payment();

        $payment->name = $request->name;
        $payment->fee = $request->fee;
        if($request->image) $payment->image = $request->image;
        if($request->description) $payment->description = $request->description;

        $payment->save();

        return response($payment, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(Payment::find($id), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\PaymentStoreUpdateRequest $request, $id)
    {
        $payment = Payment::find($id);

        if($payment)
        {
           $payment->name = $request->name;
            $payment->fee = $request->fee;
            if($request->image) $payment->image = $request->image;
            if($request->description) $payment->description = $request->description;

            $payment->save(); 

            return response(null, 200);
        }

        return response(null, 402);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Payment::destroy($id);
        return response(null, 200);
    }

}

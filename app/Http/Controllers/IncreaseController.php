<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Increase;
use App\Product;
use App\Size;

class IncreaseController extends Controller
{

    public function __construct()
    {
        $this->middleware('ability:Admin,increase.index', ['only' => ['index']]);
        $this->middleware('ability:Admin,increase.store', ['only' => ['store']]);
        $this->middleware('ability:Admin,increase.show', ['only' => ['show']]);
        $this->middleware('ability:Admin,increase.update', ['only' => ['update']]);
        $this->middleware('ability:Admin,increase.destroy', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Increase::all(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\IncreaseStoreUpdateRequest $request)
    {
        $increase = new Increase();
        if($request->product_id) $product = Product::find($request->product_id);
        if($request->size_id) $size = Size::find($request->size_id);
        if($request->increase) $increase->increase = $request->increase;
        if($request->type) $increase->type = $request->type;

        if($product != null && $size != null)
        {
            //Foreign keys
            $increase->product()->associate($product);
            $increase->size()->associate($size);
            
            $increase->save();
            return response($increase, 200);
        }
        else
        {
            return response("Invalid increase association", 402);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($productId, $sizeId)
    {
        return response(Increase::where('product_id', '=', $productId)->where('size_id', '=', $sizeId)->with(['product', 'size'])->first(), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\IncreaseStoreUpdateRequest $request, $id)
    {
        $increase = Increase::find($id);
        if($request->increase) $increase->increase = $request->increase;
        if($request->type) $increase->type = $request->type;

        $increase->save();

        return response(null, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Increase::destroy($id);
    }
}

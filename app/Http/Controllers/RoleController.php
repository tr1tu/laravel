<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Role;

class RoleController extends Controller
{

    public function __construct()
    {
        $this->middleware('ability:Admin,role.index', ['only' => ['index']]);
        $this->middleware('ability:Admin,role.store', ['only' => ['store']]);
        $this->middleware('ability:Admin,role.show', ['only' => ['show']]);
        $this->middleware('ability:Admin,role.update', ['only' => ['update']]);
        $this->middleware('ability:Admin,role.destroy', ['only' => ['destroy']]);
        $this->middleware('ability:Admin,role.clearpermissions', ['only' => ['clearPermissions']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Role::all(), 200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\RoleStoreUpdateRequest $request)
    {
        $role = new Role();
        if($request->name) $role->name = $request->name;
        if($request->display_name) $role->display_name = $request->display_name;
        if($request->description) $role->description = $request->description;
        $role->save();
        return response($role, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(Role::with('perms')->find($id), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\RoleStoreUpdateRequest $request, $id)
    {
        $role = Role::find($id);

        if($request->name) $role->name = $request->name;
        if($request->display_name) $role->display_name = $request->display_name;
        if($request->description) $role->description = $request->description;

        $role->save();

        return response(null, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Role::destroy($id);
        return response(null, 200);
    }

    //Limpia los permisos del rol recibido.
    public function clearPermissions($id)
    {
        $role = Role::find($id);
        $role->perms()->detach();

        return response(null, 200);
    }
}

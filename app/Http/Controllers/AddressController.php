<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Address;
use App\User;

use JWTAuth;

class AddressController extends Controller
{

    public function __construct()
    {
        $this->middleware('ability:Admin,address.index', ['only' => ['index']]);
        $this->middleware('ability:Admin|User,address.indexself', ['only' => ['indexSelf']]);
        $this->middleware('ability:Admin,address.store', ['only' => ['store']]);
        $this->middleware('ability:Admin,address.show', ['only' => ['show']]);
        $this->middleware('ability:Admin|User,address.showself', ['only' => ['showSelf']]);
        $this->middleware('ability:Admin,address.update', ['only' => ['update']]);
        $this->middleware('ability:Admin,address.destroy', ['only' => ['destroy']]);
        $this->middleware('ability:Admin|User,address.storeself', ['only' => ['storeSelf']]);
        $this->middleware('ability:Admin|User,address.updateself', ['only' => ['updateSelf']]);
        $this->middleware('ability:Admin|User,address.destroyself', ['only' => ['destroySelf']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Address::all(), 200);
    }

    public function indexSelf()
    {
        $user = JWTAuth::parseToken()->authenticate();

        return response($user->addresses, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\AddressStoreUpdateRequest $request)
    {
        $address = new Address();
        $user = null;

        if($request->country) $address->country = $request->country;
        if($request->province) $address->province = $request->province;
        if($request->city) $address->city = $request->city;
        if($request->zip_code) $address->zip_code = $request->zip_code;
        if($request->phone) $address->phone = $request->phone;
        if($request->mob_phone) $address->mob_phone = $request->mob_phone;
        if($request->address) $address->address = $request->address;
        if($request->description) $address->description = $request->description;
        if($request->name) $address->name = $request->name;
        if($request->lastname) $address->lastname = $request->lastname;
        if($request->dnicif) $address->dnicif = $request->dnicif;

        if($request->user_id) $user = User::find($request->user_id);

        if($user != null)
        {
            $address->user()->associate($user);
            $address->save();

            return response($address, 200);
        }
        else
        {
            return response("User not found", 402);
        }
    }


    //Users can create their own addresses.
    public function storeSelf(\App\Http\Requests\AddressStoreUpdateRequest $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        $address = new Address();

        if($request->country) $address->country = $request->country;
        if($request->province) $address->province = $request->province;
        if($request->city) $address->city = $request->city;
        if($request->zip_code) $address->zip_code = $request->zip_code;
        if($request->phone) $address->phone = $request->phone;
        if($request->mob_phone) $address->mob_phone = $request->mob_phone;
        if($request->address) $address->address = $request->address;
        if($request->description) $address->description = $request->description;
        if($request->name) $address->name = $request->name;
        if($request->lastname) $address->lastname = $request->lastname;
        if($request->dnicif) $address->dnicif = $request->dnicif;

        

        if($user != null)
        {
            $address->user()->associate($user);
            $address->save();

            return response($address, 200);
        }
        else
        {
            return response("User not found", 402);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(Address::with('user')->find($id), 200);
    }

    public function showSelf($id)
    {
        $address = Address::with('user')->find($id);
        $user = JWTAuth::parseToken()->authenticate();

        if($user->id != $address->user->id)
            return response(['unauthorized'], 403);

        return response($address, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\AddressStoreUpdateRequest $request, $id)
    {
        $address = Address::find($id);

        if($request->country) $address->country = $request->country;
        if($request->province) $address->province = $request->province;
        if($request->city) $address->city = $request->city;
        if($request->zip_code) $address->zip_code = $request->zip_code;
        if($request->phone) $address->phone = $request->phone;
        if($request->mob_phone) $address->mob_phone = $request->mob_phone;
        if($request->address) $address->address = $request->address;
        if($request->description) $address->description = $request->description;
        if($request->name) $address->name = $request->name;
        if($request->lastname) $address->lastname = $request->lastname;
        if($request->dnicif) $address->dnicif = $request->dnicif;

        $address->save();
        return response(null, 200);
    }


    //Users can modify their addresses.
    public function updateSelf(\App\Http\Requests\AddressStoreUpdateRequest $request, $id)
    {   
        $address = Address::find($id);
        $user = JWTAuth::parseToken()->authenticate();

        if($user->id != $address->user->id)
            return response(['unauthorized'], 403);


        if($request->country) $address->country = $request->country;
        if($request->province) $address->province = $request->province;
        if($request->city) $address->city = $request->city;
        if($request->zip_code) $address->zip_code = $request->zip_code;
        if($request->phone) $address->phone = $request->phone;
        if($request->mob_phone) $address->mob_phone = $request->mob_phone;
        if($request->address) $address->address = $request->address;
        if($request->description) $address->description = $request->description;
        if($request->name) $address->name = $request->name;
        if($request->lastname) $address->lastname = $request->lastname;
        if($request->dnicif) $address->dnicif = $request->dnicif;

        $address->save();
        return response(null, 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Address::destroy($id);
        return response(null, 200);
    }

    public function destroySelf($id)
    {
        $address = Address::find($id);
        $user = JWTAuth::parseToken()->authenticate();

        if($address->user->id != $user->id)
            return response(['Forbidden'], 403);

        $address->delete();
        return response(null, 200);
    }
}

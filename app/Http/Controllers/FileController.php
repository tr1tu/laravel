<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use Image;

class FileController extends Controller
{
    public function __construct() 
    {
        $this->middleware('ability:Admin,file.upload', ['only' => ['upload']]);
        $this->middleware('ability:Admin,file.download', ['only' => ['download']]);
        $this->middleware('ability:Admin,file.delete', ['only' => ['delete']]);
        $this->middleware('ability:Admin,file.getexcelfields', ['only' => ['getExcelFields']]);
        $this->middleware('ability:Admin,file.saveresizedimages', ['only' => ['saveResizedImages']]);
    }

    //Method : POST
    //Upload multiple files
    public function upload(Request $request)
    {
        $files = $request->file('files');
        $filenames = [];

        if(!$request->disk)
            $request->disk = 'local';

        foreach($files as $file)
        {
           $extension = $file->getClientOriginalExtension();
           $date = getdate();
           $dateString = $date['year'] . $date['mon'] . $date['mday'] . $date['hours'] . $date['minutes'] . $date['seconds'];
           $filename = $dateString . '_' . uniqid() . '.' . $extension;
           $filenames[] = $filename;
           Storage::disk($request->disk)->put($filename, File::get($file)); 

        }
        
        return response()->json($filenames);
        
    }

    //Method : GET
    //Download a single file
    public function download($filename)
    {
        if($filename)
        {
            $file = Storage::disk('local')->get($filename);
         
            return response($file, 200)->header('Content-Type', 'undefined');
        }
        
        return response("No filename", 402);
    }

    //Method : GET
    //Download a single file from public directory
    public function downloadPublic($filename)
    {
        if($filename)
        {
            $file = Storage::disk('public')->get($filename);
         
            return response($file, 200)->header('Content-Type', 'undefined');
        }
        
        return response("No filename", 402);
    }

    //Method : POST
    //Delete multiple files
    public function delete(Request $request)
    {
        if($request->filenames)
        {
            Storage::disk('local')->delete($filenames);
            return response(null, 200); 
        }
        
        return response('No files', 402);
    }

    //Method : POST
    //Get fields from excel file
    public function getExcelFields(Request $request)
    {
        $filename = $request->filename;

        if(!Storage::disk('local')->exists($filename))
            return response(['File not found'], 402);

        if($filename)
        {
            $data = \App::make('excel')->load('storage/app/' . $filename)->get();
            $firstrow = json_decode($data[0], true);
            $keys = array_keys($firstrow);

            //Storage::disk('local')->delete($filename);

            return response()->json($keys);
        }
    }

    //Method : POST
    //Get nodes from xml file
    public function getXmlNodeKeys(Request $request)
    {
        $filename = $request->filename;

        if(!Storage::disk('local')->exists($filename))
            return response(['File not found'], 402);

        if($filename)
        {
            $file = Storage::disk('local')->get($filename);

            $xml = simplexml_load_string($file);

            if($request->subnodes)
            {
                foreach($request->subnodes as $subnode)
                {
                    if(!isset($xml->$subnode))
                        return response(["Node not found"], 402);
                    $xml = $xml->$subnode;
                }

            }

            $keys = [];
                
            foreach($xml->children() as $key => $value)
                $keys[] = $key;

            sort($keys);


            return response()->json($keys);
        }
    }

    //Method : POST
    //Save a resized copy of an image
    public function saveResizedImages(\App\Http\Requests\FileSaveResizedImagesRequest $request)
    {
        if(!$request->width)
            $request->width = 200;
        if(!$request->height)
            $request->height = 200;

        $filenames = [];

        $files = $request->input('files');

        foreach($files as $key => $file)
        {
            $image = Image::make(Storage::disk('public')->get($file));
            $image = $image->crop($image->height(), $image->height())->resize($request->width, $request->height);
            Storage::disk('public')->put('min_' . $file, $image->stream());
            $filenames[] = 'min_' . $file;
        }

        return response()->json($filenames);
    }

}

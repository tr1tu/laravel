<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Category;

class CategoryController extends Controller
{

    public function __construct()
    {
        //$this->middleware('ability:Admin|User,category.index', ['only' => ['index']]);
        //$this->middleware('ability:Admin|User,category.indexnochildren', ['only' => ['indexNoChildren']]);
        $this->middleware('ability:Admin,category.store', ['only' => ['store']]);
        //$this->middleware('ability:Admin|User,category.show', ['only' => ['show']]);
        $this->middleware('ability:Admin,category.update', ['only' => ['update']]);
        $this->middleware('ability:Admin,category.destroy', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Category::with(['children', 'parent'])->get(), 200);
    }

    public function indexNoChildren()
    {
        return response(Category::with(['children', 'parent'])->where('parent_id','=', null)->get(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\CategoryStoreUpdateRequest $request)
    {
        $category = new Category();
        $parent = null;

        if($request->name) $category->name = $request->name;
        if($request->description) $category->description = $request->description;
        if($request->active) $category->active = $request->active;
        if($request->parent_id && $request->parent_id >= 0) 
            $parent = Category::find($request->parent_id);

        if($parent)
        {
            if($parent->parent)
                return response(["Only two levels allowed."], 402);
            if(count($category->children))
                return response(["Only two levels allowed."], 402);

            $category->parent()->associate($parent);
        }

        $category->save();

        return response($category, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(Category::with(['parent', 'children', 'tagged'])->find($id), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\CategoryStoreUpdateRequest $request, $id)
    {   
        $category = Category::find($id);
        $parent = null;

        if($request->name) $category->name = $request->name;
        if($request->description) $category->description = $request->description;
        if($request->active) $category->active = $request->active;
        if($request->parent_id && $request->parent_id >= 0) 
            $parent = Category::find($request->parent_id);

        if($parent)
        {
            if($parent->parent)
                return response(["Only two levels allowed."], 402);
            if(count($category->children))
                return response(["Only two levels allowed."], 402);

            $category->parent()->associate($parent);
        }

        if($request->parent_id && $request->parent_id < 0) 
            $category->parent()->dissociate();

        $category->save();

        return response(null, 200);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::destroy($id);
        return response(null, 200);
    }

    //POST
    public function addTag(Request $request, $id)
    {
        $category = Category::find($id);

        if($category)
        {
            if($request->tags)
            {
                $category->tag($request->tags);

                return response(null, 200);
            }
        }

        return response("No category or tags", 402);
    }

    //DELETE
    public function removeTag(Request $request, $id)
    {
        $category = Category::find($id);

        if($category)
        {
            if($request->tags)
            {
                $category->untag($request->tags);
                return response(null, 200);
            }
        }

        return response("No category or tags", 402);
    }

    //PUT
    public function setTags(Request $request, $id)
    {
        $category = Category::find($id);

        if($category)
        {
            if(isset($request->tags))
            {
                $category->retag($request->tags);
                return response($category, 200);
            }
        }

        return response("No category or tags", 402);
    }
}

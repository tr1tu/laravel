<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Stock;
use App\Product;
use App\Size;

class StockController extends Controller
{

    public function __construct()
    {
        $this->middleware('ability:Admin,stock.index', ['only' => ['index']]);
        $this->middleware('ability:Admin,stock.store', ['only' => ['store']]);
        $this->middleware('ability:Admin,stock.show', ['only' => ['show']]);
        $this->middleware('ability:Admin,stock.update', ['only' => ['update']]);
        $this->middleware('ability:Admin,stock.destroy', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Stock::all(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\StockStoreUpdateRequest $request)
    {
        $stock = new Stock();
        if($request->product_id) $product = Product::find($request->product_id);
        if($request->size_id) $size = Size::find($request->size_id);
        if($request->stock) $stock->stock = $request->stock;

        if($product != null && $size != null)
        {
            //Foreign keys
            $stock->product()->associate($product);
            $stock->size()->associate($size);

            $stock->save();
            return response($stock, 200);
        }
        else
        {
            return response("Invalid stock association", 402);
        }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($productId, $sizeId)
    {
        return response(Stock::where('product_id', '=', $productId)->where('size_id', '=', $sizeId)->with(['product', 'size'])->first(), 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\StockStoreUpdateRequest $request, $id)
    {
        $stock = Stock::find($id);
        if($request->stock) $stock->stock = $request->stock;
        $stock->save();
        return response(null, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($productId, $sizeId)
    {
        Stock::find($id);
        return response(null, 200);
    }
}

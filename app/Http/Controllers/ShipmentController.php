<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Shipment;

class ShipmentController extends Controller
{

    public function __construct() 
    {
        $this->middleware('ability:Admin|User,shipment.index', ['only' => ['index']]);
        $this->middleware('ability:Admin,shipment.store', ['only' => ['store']]);
        $this->middleware('ability:Admin|User,shipment.show', ['only' => ['show']]);
        $this->middleware('ability:Admin,shipment.update', ['only' => ['update']]);
        $this->middleware('ability:Admin,shipment.destroy', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Shipment::all(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\ShipmentStoreUpdateRequest $request)
    {
        $shipment = new Shipment();

        $shipment->name = $request->name;
        $shipment->fee = $request->fee;
        if($request->image) $shipment->image = $request->image;
        if($request->description) $shipment->description = $request->description;

        $shipment->save();

        return response($shipment, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(Shipment::find($id), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\ShipmentStoreUpdateRequest $request, $id)
    {
        $shipment = Shipment::find($id);

        if($shipment)
        {
           $shipment->name = $request->name;
            $shipment->fee = $request->fee;
            if($request->image) $shipment->image = $request->image;
            if($request->description) $shipment->description = $request->description;

            $shipment->save(); 

            return response(null, 200);
        }

        return response(null, 402);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Shipment::destroy($id);

        return response(null, 200);
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//CORS

/*header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers', 'Content-Type, Accept, Authorization, X-Requested-With');
*/
Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});



Route::group(['prefix' => 'api','middleware' =>'cors' ], function()
{
	//Resource controllers
	Route::resource('address', 'AddressController');
	Route::resource('color', 'ColorController');
	Route::resource('discount', 'DiscountController');
	Route::resource('product', 'ProductController');
	Route::resource('size', 'SizeController');
	Route::resource('sizing', 'SizingController');
	Route::resource('tax', 'TaxController');	
	Route::resource('user', 'UserController');
	Route::resource('role', 'RoleController');
	Route::resource('permission', 'PermissionController');
	Route::resource('image', 'ImageController');
	Route::resource('category', 'CategoryController');
	Route::resource('opinion', 'OpinionController');
	Route::resource('shipment', 'ShipmentController');
	Route::resource('payment', 'PaymentController');
	Route::resource('cart', 'CartController');
	Route::resource('order', 'OrderController');
	Route::resource('ticket', 'TicketController');
	Route::resource('ticketcategory', 'TicketCategoryController');
	Route::resource('ticketpriority', 'TicketPriorityController');
	Route::resource('ticketanswer', 'TicketAnswerController');
	Route::resource('importprofile', 'ImportProfileController');
	Route::resource('operation', 'OperationController');
	Route::resource('slide', 'SlideController');
	Route::resource('importvariable', 'ImportVariableController');

	//User & Address custom routes.
	Route::put('me/user', 'UserController@updateSelf');
	Route::get('me/user', 'UserController@showSelf');
	Route::post('register', 'UserController@register');

	Route::post('me/address', 'AddressController@storeSelf');
	Route::put('me/address/{id}', 'AddressController@updateSelf');
	Route::get('me/address/{id}', 'AddressController@showSelf');
	Route::get('me/address', 'AddressController@indexSelf');
	Route::delete('me/address/{id}', 'AddressController@destroySelf');
	Route::delete('me/user', 'UserController@destroySelf');
    Route::post('authenticate', 'AuthenticateController@authenticate');
    Route::get('authenticate/user', 'AuthenticateController@getAuthenticatedUser');	
    
	// Route to create a new role
	Route::post('createrole', 'AuthenticateController@createRole');
	// Route to create a new permission
	Route::post('createpermission', 'AuthenticateController@createPermission');
	// Route to assign role to user
	Route::post('assign-role', 'AuthenticateController@assignRole');
	// Route to attache permission to a role
	Route::post('attach-permission', 'AuthenticateController@attachPermission');
	//Clear role permissions
	Route::post('clear-permissions/{id}', 'RoleController@clearPermissions');
	//Clear user roles
	Route::post('clear-roles/{id}', 'UserController@clearRoles');
	
	//Stock routes
	Route::get('stock', ['as' => 'api.stock.index', 'uses' => 'StockController@index']);
	Route::post('stock', ['as' => 'api.stock.store', 'uses' => 'StockController@store']);
	Route::get('stock/{product_id}/{size_id}', ['as' => 'api.stock.show', 'uses' => 'StockController@show']);
	Route::put('stock/{id}', ['as' => 'api.stock.update', 'uses' => 'StockController@update']);
	Route::delete('stock/{id}', ['as' => 'api.stock.destroy', 'uses' => 'StockController@destroy']);

	//Increase routes
	Route::get('increase', ['as' => 'api.increase.index', 'uses' => 'IncreaseController@index']);
	Route::post('increase', ['as' => 'api.increase.store', 'uses' => 'IncreaseController@store']);
	Route::get('increase/{product_id}/{size_id}', ['as' => 'api.increase.show', 'uses' => 'IncreaseController@show']);
	Route::put('increase/{id}', ['as' => 'api.increase.update', 'uses' => 'IncreaseController@update']);
	Route::delete('increase/{id}', ['as' => 'api.increase.destroy', 'uses' => 'IncreaseController@destroy']);

	//Product paginate
	Route::get('product-page/{page?}/{field?}/{type?}', ['as' => 'api.product.getPage', 'uses' => 'ProductController@getPage']);
	Route::get('product-cat-page/{catId}/{page?}/{field?}/{type?}', ['as' => 'apo.product.getPageCategory', 'uses' => 'ProductController@getPageCategory']);
	Route::post('product-cat-tag-page', ['as' => 'api.product.getPageCatTags', 'uses' => 'ProductController@getPageCatTags']);
	Route::post('product-tag-page', ['as' => 'api.product.getPageTags', 'uses' => 'ProductController@getPageTags']);
	Route::post('product-search', ['as' => 'api.product.search', 'uses' => 'ProductController@search']);

	//Product tags
	Route::post('product-add-tag/{id}', ['as' => 'api.product.addtag', 'uses' => 'ProductController@addTag']);
	Route::delete('product-remove-tag/{id}', ['as' => 'api.product.removetag', 'uses' => 'ProductController@removeTag']);
	Route::put('product-set-tag/{id}', ['as' => 'api.product.settag', 'uses' => 'ProductController@setTags']);

	//Category tags
	Route::post('category-add-tag/{id}', ['as' => 'api.category.addtag', 'uses' => 'CategoryController@addTag']);
	Route::delete('category-remove-tag/{id}', ['as' => 'api.category.removetag', 'uses' => 'CategoryController@removeTag']);
	Route::put('category-set-tag/{id}', ['as' => 'api.category.settag', 'uses' => 'CategoryController@setTags']);

	//Category custom
	Route::get('category-no-children', ['as' => 'api.category.indexnochildren', 'uses' => 'CategoryController@indexNoChildren']);

	//Files management
	Route::post('upload-file', ['as' => 'api.file.upload', 'uses' => 'FileController@upload']);
	Route::get('download-file/{filename}', ['as' => 'api.file.download', 'uses' => 'FileController@download']);
	Route::get('download-filep/{filename}', ['as' => 'api.file.downloadpublic', 'uses' => 'FileController@downloadPublic']);
	Route::post('delete-file', ['as' => 'api.file.delete', 'uses' => 'FileController@delete']);
	Route::post('get-excel-fields', ['as' => 'api.file.getexcelfields', 'uses' => 'FileController@getExcelFields']);
	Route::post('get-xml-node-keys', ['as' => 'api.file.getxmlnodekeys', 'uses' => 'FileController@getXmlNodeKeys']);
	Route::post('save-resized-images', ['as' => 'api.file.saveresizedimages', 'uses' => 'FileController@saveResizedImages']);

	//Image custom
	Route::delete('image-clear-order/{id}', ['as' => 'api.image.clearorder', 'uses' => 'ImageController@clearOrder']);

	//Product import
	Route::post('product-import', ['as' => 'api.product.import', 'uses' => 'ProductController@import']);
	Route::post('product-parse', ['as' => 'api.product.parse', 'uses' => 'ProductController@parseXml']);

	//Related products
	Route::post('product-relate', ['as' => 'api.product.relate', 'uses' => 'ProductController@relate']);
	Route::get('get-related/{id}/{n}', ['as' => 'api.product.getRelated', 'uses' => 'ProductController@getRelated']);

	//Opinion custom routes
	Route::get('opinion-product/{id}/{pagesize}/{orderfield}/{ordertype}', ['as' => 'api.opinion.indexproduct', 'uses' => 'OpinionController@indexProduct']);
	Route::get('opinion-user/{id}', ['as' => 'api.opinion.indexuser', 'uses' => 'OpinionController@indexUser']);
	Route::get('opinion-user-product/{userid}/{productid}', ['as' => 'api.opinion.indexuserproduct', 'uses' => 'OpinionController@indexUserProduct']);
	Route::put('opinion-self/{id}', ['as' => 'api.opinion.updateself', 'uses' => 'OpinionController@updateSelf']);
	Route::delete('opinion-self/{id}', ['as' => 'api.opinion.destroyself', 'uses' => 'OpinionController@destroySelf']);
	Route::post('opinion-self', ['as' => 'api.opinion.storeself', 'uses' => 'OpinionController@storeSelf']);
	Route::post('opinion-upvote', ['as' => 'api.opinion.upvote', 'uses' => 'OpinionController@upvote']);
	Route::post('opinion-downvote', ['as' => 'api.opinion.downvote', 'uses' => 'OpinionController@downvote']);

	//Cart custom routes
	Route::post('cart-get/', ['as' => 'api.cart.getcart', 'uses' => 'CartController@getCart']);
	Route::post('cart-add/', ['as' => 'api.cart.addtocart', 'uses' => 'CartController@addToCart']);
	Route::post('cart-copy/', ['as' => 'api.cart.copycart', 'uses' => 'CartController@copyCart']);

	//Order custom routes
	Route::post('order-from-cart/', ['as' => 'api.order.additemsfromcart', 'uses' => 'OrderController@addItemsFromCart']);
	Route::get('my-order/{id}', ['as' => 'api.order.showself', 'uses' => 'OrderController@showSelf']);
	Route::get('my-orders/', ['as' => 'api.order.indexSelf', 'uses' => 'OrderController@indexSelf']);
	Route::post('finish-payment/', ['as' => 'api.order.finishpayment', 'uses' => 'OrderController@finishPayment']);
	Route::get('paypalpay/', ['as' => 'api.order.paypalpay', 'uses' => 'OrderController@paypalPay']);
	Route::put('order-status/{id}', ['as' => 'api.order.updatestatus', 'uses' => 'OrderController@updateStatus']);
	Route::post('order/customer-email', ['as' => 'api.order.sendcustomeremail', 'uses' => 'OrderController@sendCustomerEmail']);

	//Config routes
	Route::get('company-data/', ['as' => 'api.config.getcompanydata', 'uses' => 'ConfigController@getCompanyData']);
	Route::get('order-status/', ['as' => 'api.config.getorderstatus', 'uses' => 'ConfigController@getOrderStatus']);
	Route::get('order-status-cod/', ['as' => 'api.config.getorderstatuscod', 'uses' => 'ConfigController@getOrderStatusCOD']);
	Route::get('social-media/', ['as' => 'api.config.getsocialmedia', 'uses' => 'ConfigController@getSocialMedia']);
	Route::get('payment-fees/', ['as' => 'api.config.getpaymentfees', 'uses' => 'ConfigController@getPaymentFees']);
	Route::get('payment-f-steps/', ['as' => 'api.config.getpaymentfinishedsteps', 'uses' => 'ConfigController@getPaymentFinishedSteps']);
	Route::post('config-set/', ['as' => 'api.config.setmulti', 'uses' => 'ConfigController@setMulti']);
	Route::get('config/', ['as' => 'api.config.index', 'uses' => 'ConfigController@index']);

	//Ticket custom routes
	Route::get('ticket-self/', ['as' => 'api.config.ticketindexself', 'uses' => 'TicketController@indexSelf']);
	Route::get('ticket-self/{id}', ['as' => 'api.config.ticketshowself', 'uses' => 'TicketController@showSelf']);
	Route::put('ticket-self/{id}', ['as' => 'api.config.ticketupdateself', 'uses' => 'TicketController@updateSelf']);
	Route::delete('ticket-self/{id}', ['as' => 'api.config.ticketcancelself', 'uses' => 'TicketController@cancelSelf']);

	//Slides
	Route::get('slide-active/', ['as' => 'api.slide.indexactive', 'uses' => 'SlideController@indexActive']);
	Route::post('slide-store-multi/', ['as' => 'api.slide.storemulti', 'uses' => 'SlideController@storeMulti']);

});
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Increase extends Model
{
    protected $fillable = [
    	'increase', 'type'
    ];

    protected $hidden = [];

    public function product()
    {
    	return $this->belongsTo('App\Product');
    }

    public function size()
    {
    	return $this->belongsTo('App\Size');
    }
}

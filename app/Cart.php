<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $fillable = [];

    protected $hidden = [];

    protected $appends = ['price_total'];

    private $priceTotal_;

    public function products()
    {
    	return $this->belongsToMany('App\Product')->withPivot(['ammount']);
    }

    public function getPriceTotalAttribute()
    {
    	if(! isset($this->priceTotal_))
    	{
    		$this->calculateTotal();
    	}

    	return $this->priceTotal_;

    }

    private function calculateTotal()
    {
    	$this->priceTotal_ = 0;

    	foreach($this->products as $product)
    	{
    		$this->priceTotal_ += $product->price_total * $product->pivot->ammount;
    	}
    }
}

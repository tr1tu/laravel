<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Opinion extends Model
{
    protected $fillable = [
    	'comments', 'positive', 'negative', 'recommended', 'score', 'created_at'
    ];

    protected $hidden = [];

    protected $appends = ['upvotes', 'downvotes'];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function product()
    {
    	return $this->belongsTo('App\Product');
    }

    public function voters()
    {
        return $this->belongsToMany('App\User', 'opinion_votes')->withPivot('value')->select(['id']);
    }

    public function getUpvotesAttribute()
    {
        return $this->calculateVotes(1);
    }

    public function getDownvotesAttribute()
    {
        return $this->calculateVotes(0);
    }

    public function scopeFromUser($query, $id)
    {
    	return $query->where('user_id', '=', $id);
    }

    public function scopeFromProduct($query, $id)
    {
    	return $query->where('product_id', '=', $id);
    }

    public function scopeWithUsername($query)
    {
        return $query->with(['user' => function($query) {
            $query->select(['id', 'username']);
        }]);
    }

    public function scopeWithVotes($query)
    {
        $query = $query->with(['voters' => function($q) {
            $q->select(['id']);
        }]);
        $query->upvotes;
        $query->downvotes;
        return $query;
    }

    private static function vote($opinionId, $userId, $v)
    {
        $opinion = Opinion::find($opinionId);

        if($opinion)
        {
            $opinion->voters()->detach($userId);
            $opinion->voters()->attach($userId, ['value' => $v]);
        }

        return $opinion;
    }

    public static function upvote($opinionId, $userId)
    {
        return Opinion::vote($opinionId, $userId, 1);
    }

    public static function downvote($opinionId, $userId)
    {
        return Opinion::vote($opinionId, $userId, 0);
    }

    public function calculateVotes($value)
    {
        $votes = 0;

        foreach($this->voters as $voter)
        {
            if($voter->pivot->value == $value)
                $votes++;
        }

        return $votes;
    }

}

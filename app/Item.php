<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
    	'code', 'ean', 'units_per_pack', 'price', 'ammount', 'name'
    ];

    //NOTE: price attribute includes discounts.

    protected $hidden = [];

    protected $appends = ['price_total'];

    private $priceTotal_;

    public function order()
    {
    	return $this->belongsTo('App\Order');
    }

    public function product()
    {
    	return $this->belongsTo('App\Product');
    }

    public function itemTaxes()
    {
    	return $this->hasMany('App\ItemTax');
    }

    public function getPriceTotalAttribute()
    {
    	if( ! $this->priceTotal_ )
    	{
    		$total = $this->price*1;

	    	foreach($this->itemTaxes as $tax)
	    	{
	    		$total += ($tax->tax/100) * $this->price;
	    	}

	    	$this->priceTotal_ = $total;
    	}
    	
    	return $this->priceTotal_;
    }
}

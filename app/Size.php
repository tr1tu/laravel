<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    protected $fillable = [
    	'size'
    ];

    protected $hidden = [];

    public function sizing()
    {
    	return $this->belongsTo('App\Sizing');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use \Conner\Tagging\Taggable;

    protected $fillable = [
    	'name', 'description', 'active'
    ];

    protected $hidden = [];

    public function parent()
    {
    	return $this->belongsTo('App\Category', 'parent_id');

    }

    public function children()
    {
    	return $this->hasMany('App\Category', 'parent_id');
    }

    public function products()
    {
    	return $this->hasMany('App\Product');
    }

}

<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Support\Facades\Hash; //Password

use App\Role;
use App\Cart; // Create new cart on user creation.

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {
    
    use EntrustUserTrait;
    use Authenticatable, CanResetPassword;

   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'password', 'salt', 'name', 
        'lastname', 'dnicif', 'phone',
        'mobphone', 'fax', 'activated', 'country',
        'email', 'expired_password'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'salt'
    ];

    public function addresses()
    {
        return $this->hasMany('App\Address');
    }


    public function opinions()
    {
        return $this->hasMany('App\Opinion');
    }

    public function votes()
    {
        return $this->belongsToMany('App\Opinion','opinion_votes')->withPivot('value');
    }

    public function cart()
    {
        return $this->belongsTo('App\Cart');
    }

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function tickets()
    {
        return $this->hasMany('App\Ticket');
    }

    public function agentTickets()
    {
        return $this->hasMany('App\Ticket', 'agent_id');
    }

    public function ticketAnswers()
    {
        return $this->hasMany('App\TicketAnswer');
    }

    public static function setPassword($user, $password)
    {
        if(!is_a($user, 'App\User'))
            return;

        $user->salt = base64_encode(hash('tiger192,4', uniqid(mt_rand(), true), true)); //32 bytes length
        $user->password = Hash::make($password . $user->salt);

    }

    //Overriding save method in order to put new users in the 'User' group.
    public function save(array $options = array())
    {
        $isNew = !isset($this->id);

        if($isNew && ! $r = Role::where('name', 'User')->first())
        {
            $r = new Role;
            $r->name = "User";
            $r->display_name = "User";
            $r->description = "Normal user";
            $r->save();
        }

        $cart = new Cart();
        $cart->save();

        $this->cart()->associate($cart);
        
        parent::save($options);

        if($isNew)
        {
            $this->roles()->attach($r);
        }
            

    }

}

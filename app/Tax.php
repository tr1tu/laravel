<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
    protected $fillable = [
    	'tax', 'name'
    ];

    protected $hidden = [];

    public function products()
    {
    	return $this->belongsToMany('App\Product');
    }

}

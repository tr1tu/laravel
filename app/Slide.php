<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DateTime;

class Slide extends Model
{
    protected $fillable = [
        'title', 'text', 'image_url',
        'active', 'order', 'start_date',
        'end_date', 'link'
    ];

    protected $hidden = [];

    protected $appends = ['shown'];

    public function scopeGetActive($query)
    {
        return $query   ->where('start_date', '<=', new DateTime('now'))
                        ->where('end_date', '>=', new DateTime('now'))
                        ->where('active', '=', true);
    }

    public function getShownAttribute()
    {
        $now = new DateTime('now');
        $start = new DateTime($this->start_date);
        $end = new DateTime($this->end_date);
        return $this->active && $start <= $now && $end >= $now;
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemTax extends Model
{
    protected $fillable = [
    	'tax', 'name'
    ];

    protected $hidden = [];

    public function item()
    {
    	return $this->belongsTo('App\Item');
    }

}

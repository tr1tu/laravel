<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImportProfile extends Model
{
    protected $fillable = ['name', 'supnodes', 'name_format', 'description_format',
        'price_format', 'stock_format', 'reference_format', 'category_format',
        'code_format', 'ean_format', 'trading_price_format', 'units_per_pack_format',
        'manufacturer_format', 'discountinued_format', 'image_url_format', 'small_image_url_format'
    ];

    protected $hidden = [];

    protected $appends = ['settings_array'];

    public function operations()
    {
        return $this->belongsToMany('App\Operation')->withPivot('order');
    }

    public function importSettings()
    {
        return $this->hasMany('App\ImportSetting');
    }

    public function taxes()
    {
        return $this->belongsToMany('App\Tax');
    }

    public function discounts()
    {
        return $this->belongsToMany('App\Discount');
    }

    public function setSetting($key, $value)
    {
        $setting = $this->importSettings()->firstOrNew(['key' => $key, 'import_profile_id' => $this->id]);
        $setting->value = $value;
        $setting->save();

        //Unset importSettings so it will be reloaded next time it is called.
        unset($this->importSettings); 

        return $this;
    }

    public function unsetSetting($key)
    {
        $this->importSettings()->where('key',$key)->delete();

        //Unset importSettings so it will be reloaded next time it is called.
        unset($this->importSettings);

        return $this;
    }

    public function getSettingsArrayAttribute()
    {
        $settings = $this->importSettings()->get();
        $arr = [];

        foreach($settings as $s)
        {
            $arr[$s->key] = $s->value;
        }

        return (object)$arr;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipment extends Model
{

    protected $fillable = [
    	'name', 'fee', 'image', 'description'
    ];

    protected $hidden = [];

    protected $dates = ['deleted_at'];

    public function orders()
    {
    	return $this->hasMany('App\Orders');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [];

    protected $hidden = [];

    public function order()
    {
    	return $this->belongsTo('App\Order');
    }
}

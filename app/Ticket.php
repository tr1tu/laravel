<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = [
    	'message', 'status'
    ];

    protected $hidden = [];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function agent()
    {
    	return $this->belongsTo('App\User');
    }

    public function ticketCategory()
    {
    	return $this->belongsTo('App\TicketCategory');
    }

    public function ticketPriority()
    {
    	return $this->belongsTo('App\TicketPriority');
    }

    public function ticketAnswers()
    {
        return $this->hasMany('App\TicketAnswer');
    }

    public static function setStatusCancelled($ticket)
    {
        $ticket->status = 0;
    }

    public static function setStatusWaiting($ticket)
    {
    	$ticket->status = 1;
    }

 	public static function setStatusAnswered($ticket)
    {
    	$ticket->status = 2;
    }

    public static function setStatusSolved($ticket)
    {
    	$ticket->status = 3;
    }

    public function getStatusAttribute($value)
    {
    	$text = \App\Config::get('ticket_status_text_' . $value);
    	return [ 'id' => $value, 'name' => isset($text) ? $text : '' ];
    }

       
}

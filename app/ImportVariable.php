<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImportVariable extends Model
{
    protected $fillable = [
        'name', 'value'
    ];

    protected $hidden = []; 


    public static function get($name)
    {
    	$variable = self::where('name', $name)->first();

    	if($variable)
    		return $variable->value;
    	else
    		return null;
    }

    public static function getMulti($names = array())
    {
        if(count($names) > 0)
        {
            $variables = self::where('name', $names[0]);

            for($i = 1; $i < count($names); $i++)
            {
                $variables = $variables->orWhere('name', $names[$i]);
            }

            $variables = $variables->get();

            $values = array();
            foreach($variables as $variable)
            {
                $values[$variable->name] = $variable->value;
            }

            return $values;
        }

        return null;
    }

    public static function set($name, $value)
    {
    	$variable = self::where('name', $name)->first();

    	if($variable)
    	{
    		$variable->value = $value;
    		$variable->save();
    	}
    	else
		{
			$variable = new ImportVariable();
			$variable->name = $name;
			$variable->value = $value;
			$variable->save();	
		}

		return $variable;
    }
}

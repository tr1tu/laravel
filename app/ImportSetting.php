<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImportSetting extends Model
{
    protected $fillable = [
        'key', 'value'
    ];

    protected $hidden = [];

    public function importProfile()
    {
        return $this->belongsTo('App\ImportProfile');
    }

}
